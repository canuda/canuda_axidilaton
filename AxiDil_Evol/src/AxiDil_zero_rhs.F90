#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Functions.h"
#include "cctk_Parameters.h"

subroutine AxiDil_zero_rhs( CCTK_ARGUMENTS )

  implicit none
  DECLARE_CCTK_ARGUMENTS
  DECLARE_CCTK_FUNCTIONS
  DECLARE_CCTK_PARAMETERS

  rhs_Phi_gf  = 0
  rhs_KPhi_gf = 0
  rhs_Theta_gf  = 0
  rhs_KTheta_gf = 0

end subroutine AxiDil_zero_rhs
