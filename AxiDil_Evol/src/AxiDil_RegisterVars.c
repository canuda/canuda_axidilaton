
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

void AxiDil_RegisterVars(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INT ierr = 0, group, rhs;

  // register evolution and rhs gridfunction groups with MoL

  /* ADM metric and extrinsic curvature */
  group = CCTK_GroupIndex("ADMBase::lapse");
  ierr += MoLRegisterSaveAndRestoreGroup(group);
  group = CCTK_GroupIndex("ADMBase::shift");
  ierr += MoLRegisterSaveAndRestoreGroup(group);
  group = CCTK_GroupIndex("ADMBase::metric");
  ierr += MoLRegisterSaveAndRestoreGroup(group);
  group = CCTK_GroupIndex("ADMBase::curv");
  ierr += MoLRegisterSaveAndRestoreGroup(group);

  /* Dilaton scalar field */
  group = CCTK_GroupIndex("AxiDil_Base::Phi_group");
  rhs   = CCTK_GroupIndex("AxiDil_Evol::rhs_Phi_group");
  ierr += MoLRegisterEvolvedGroup(group, rhs);

  /* Dilaton scalar field momentum */
  group = CCTK_GroupIndex("AxiDil_Base::KPhi_group");
  rhs   = CCTK_GroupIndex("AxiDil_Evol::rhs_KPhi_group");
  ierr += MoLRegisterEvolvedGroup(group, rhs);

 /* Axion scalar field */
  group = CCTK_GroupIndex("AxiDil_Base::Theta_group");
  rhs   = CCTK_GroupIndex("AxiDil_Evol::rhs_Theta_group");
  ierr += MoLRegisterEvolvedGroup(group, rhs);

  /* Axion scalar field momentum */
  group = CCTK_GroupIndex("AxiDil_Base::KTheta_group");
  rhs   = CCTK_GroupIndex("AxiDil_Evol::rhs_KTheta_group");
  ierr += MoLRegisterEvolvedGroup(group, rhs);

  if (ierr) CCTK_ERROR("Problems registering with MoL");

}
