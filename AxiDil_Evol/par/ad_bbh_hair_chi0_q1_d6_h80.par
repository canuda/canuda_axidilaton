#------------------------------------------------------------------------------
ActiveThorns = "
  ADMBase
  ADMCoupling
  ADMMacros
  AEILocalInterp
  AHFinderDirect
  AxiDil_Base
  AxiDil_Init
  AxiDil_Evol
  Boundary
  Carpet
  CarpetIOASCII
  CarpetIOBasic
  CarpetIOHDF5
  CarpetIOScalar
  CarpetInterp
  CarpetLib
  CarpetMask
  CarpetReduce
  CarpetRegrid2
  CarpetTracker
  CartGrid3D
  CoordBase
  CoordGauge
  Dissipation
  Fortran
  GenericFD
  GSL
  HDF5
  InitBase
  IOUtil
  KerrQuasiIsotropic
  LeanBSSNMoL
  LocalInterp
  LoopControl
  MoL
  Multipole
  NaNChecker
  NewRad
  NPScalars
  PunctureTracker
  QuasiLocalMeasures
  ReflectionSymmetry
  Slab
  SpaceMask
  SphericalSurface
  StaticConformal
  SymBase
  SystemStatistics
  TerminationTrigger
  Time
  TimerReport
  TmunuBase
  TwoPunctures
"
#------------------------------------------------------------------------------


#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
# Grid setup
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
CartGrid3D::type                     = "coordbase"
Carpet::domain_from_coordbase        = yes
CoordBase::domainsize                = "minmax"

# make sure all (xmax - xmin)/dx are integers!
CoordBase::xmin                      =-256.0
CoordBase::ymin                      =-256.0
CoordBase::zmin                      =-256.0
CoordBase::xmax                      =+256.0
CoordBase::ymax                      =+256.0
CoordBase::zmax                      =+256.0
CoordBase::dx                        =   1.6
CoordBase::dy                        =   1.6
CoordBase::dz                        =   1.6

driver::ghost_size                   = 3

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_z_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_upper     = 3

CoordBase::boundary_shiftout_x_lower = 0
CoordBase::boundary_shiftout_y_lower = 0
CoordBase::boundary_shiftout_z_lower = 0

ReflectionSymmetry::reflection_x     = no 
ReflectionSymmetry::reflection_y     = no 
ReflectionSymmetry::reflection_z     = no 
ReflectionSymmetry::avoid_origin_x   = yes
ReflectionSymmetry::avoid_origin_y   = yes
ReflectionSymmetry::avoid_origin_z   = yes

Time::dtfac                             = 0.25
Carpet::time_refinement_factors         = "[1, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512]"

#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
# Mesh refinement
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
Carpet::max_refinement_levels           = 8

CarpetRegrid2::num_centres              = 2

CarpetRegrid2::num_levels_1             = 8
CarpetRegrid2::position_x_1             = 3
CarpetRegrid2::radius_1[1]              =128.0 
CarpetRegrid2::radius_1[2]              = 32.0 
CarpetRegrid2::radius_1[3]              = 16.0 
CarpetRegrid2::radius_1[4]              =  6.0 
CarpetRegrid2::radius_1[5]              =  3.0 
CarpetRegrid2::radius_1[6]              =  1.5
CarpetRegrid2::radius_1[7]              =  0.75 
CarpetRegrid2::movement_threshold_1     = 0.16

CarpetRegrid2::num_levels_2             = 8
CarpetRegrid2::position_x_2             = -3
CarpetRegrid2::radius_2[1]              =128.0 
CarpetRegrid2::radius_2[2]              = 32.0 
CarpetRegrid2::radius_2[3]              = 16.0 
CarpetRegrid2::radius_2[4]              =  6.0 
CarpetRegrid2::radius_2[5]              =  3.0 
CarpetRegrid2::radius_2[6]              =  1.5
CarpetRegrid2::radius_2[7]              =  0.75 
CarpetRegrid2::movement_threshold_2     = 0.16

Carpet::use_buffer_zones                = yes
Carpet::prolongation_order_space        = 5
Carpet::prolongation_order_time         = 2

CarpetRegrid2::regrid_every             = 64
CarpetRegrid2::freeze_unaligned_levels  = yes
CarpetRegrid2::verbose                  = no

Carpet::grid_structure_filename         = "carpet-grid-structure"
Carpet::grid_coordinates_filename       = "carpet-grid-coordinates"


# Initial Data
#------------------------------------------------------------------------------

ADMBase::initial_data                 = "TwoPunctures"
ADMBase::initial_lapse                = "psi^n"
ADMBase::initial_shift                = "zero"
ADMBase::initial_dtlapse              = "zero"
ADMBase::initial_dtshift              = "zero"

ADMBase::lapse_timelevels             = 3
ADMBase::shift_timelevels             = 3
ADMBase::metric_timelevels            = 3

TwoPunctures::initial_lapse_psi_exponent = -2.0 ###

TwoPunctures::give_bare_mass            = no
TwoPunctures::target_M_plus             = 0.5 
TwoPunctures::target_M_minus            = 0.5 
TwoPunctures::par_m_plus              = 0.476534633024028
TwoPunctures::par_m_minus             = 0.476534633024028

TwoPunctures::par_b                     = 3.0
TwoPunctures::center_offset[0]          = 0.0 

TwoPunctures::par_P_plus[0]           = -0.0058677669328272
TwoPunctures::par_P_plus[1]           = 0.138357448824906
TwoPunctures::par_P_plus[2]           = 0.

TwoPunctures::par_P_minus[0]          = 0.0058677669328272
TwoPunctures::par_P_minus[1]          = -0.138357448824906
TwoPunctures::par_P_minus[2]          = 0.

TwoPunctures::par_S_plus[0]             = 0.0
TwoPunctures::par_S_plus[1]             = 0.0
TwoPunctures::par_S_plus[2]             = 0.0

TwoPunctures::par_S_minus[0]            = 0.0
TwoPunctures::par_S_minus[1]            = 0.0
TwoPunctures::par_S_minus[2]            = 0.0

TwoPunctures::grid_setup_method         = "evaluation"
TwoPunctures::give_bare_mass            = no
TwoPunctures::TP_epsilon                = 1.0d-06
TwoPunctures::TP_Tiny                   = 1.0d-10 ###

TwoPunctures::npoints_A                 = 30         ###
TwoPunctures::npoints_B                 = 30         ###
TwoPunctures::npoints_phi               = 16          ###
TwoPunctures::Newton_maxit              = 12         ###
TwoPunctures::Newton_tol                = 1.0e-10    ###

TwoPunctures::keep_u_around             = no   ###
TwoPunctures::verbose                   = no   ###

AxiDil_Base::AxiDil_initdata      = "ID_AD_Zero"
#AxiDil_Base::AxiDil_initdata      = "ID_AD_Axidilaton_BBH"
#AxiDil_Init::AxiDil_initdata_dil  = "Dilaton_HairSlowRot_BBH"
#AxiDil_Init::AxiDil_initdata_axi  = "Axion_HairSlowRot_BBH"
#AxiDil_Init::m_plus               = 0.5
#AxiDil_Init::m_minus              = 0.5
#AxiDil_Init::chi_plus             = 0
#AxiDil_Init::chi_minus            = 0
#AxiDil_Init::pos_plus[0]          = +3.0
#AxiDil_Init::pos_minus[0]         = -3.0
#AxiDil_Init::eps_r                = 1e-6

InitBase::initial_data_setup_method   = "init_some_levels"
Carpet::init_fill_timelevels          = yes
Carpet::init_3_timelevels             = no

# #------------------------------------------------------------------------------
# #------------------------------------------------------------------------------
# Spacetime Evolution
# #------------------------------------------------------------------------------
# #------------------------------------------------------------------------------

ADMBase::evolution_method             = "LeanBSSNMoL"
ADMBase::lapse_evolution_method       = "LeanBSSNMoL"
ADMBase::shift_evolution_method       = "LeanBSSNMoL"
ADMBase::dtlapse_evolution_method     = "LeanBSSNMoL"
ADMBase::dtshift_evolution_method     = "LeanBSSNMoL"

#------------------------------------------
# Gauge parameters - lapse
#------------------------------------------
LeanBSSNMoL::slicing_condition        = "1+log"
LeanBSSNMoL::zeta_alpha               = 1
LeanBSSNMoL::kappa_alpha              = 2.0
LeanBSSNMoL::precollapsed_lapse       = no

#------------------------------------------
# Gauge parameters - shift
#------------------------------------------
LeanBSSNMoL::zeta_beta                = 1
LeanBSSNMoL::beta_Alp                 = 0
LeanBSSNMoL::eta_beta                 = 1
LeanBSSNMoL::beta_Gamma               = 0.75
LeanBSSNMoL::rescale_shift_initial    = no

#------------------------------------------
# Gauge parameters - gammat
#------------------------------------------
LeanBSSNMoL::chi_gamma                = 0

#------------------------------------------
# Advection parameters
#------------------------------------------
LeanBSSNMoL::zeta_ww                  = 1
LeanBSSNMoL::zeta_hh                  = 1
LeanBSSNMoL::zeta_gammat              = 1
LeanBSSNMoL::zeta_trk                 = 1
LeanBSSNMoL::zeta_aa                  = 1

#------------------------------------------
# Other evolution parameters
#------------------------------------------
LeanBSSNMoL::impose_conf_fac_floor_at_initial         = yes
LeanBSSNMoL::conf_fac_floor        = 1.0d-04
LeanBSSNMoL::eps_r                 = 1.0d-06

LeanBSSNMoL::derivs_order             = 4
LeanBSSNMoL::use_advection_stencils   = "yes"
LeanBSSNMoL::reset_dethh              = "no"
LeanBSSNMoL::make_aa_tracefree        = "yes"
LeanBSSNMoL::z_is_radial              = "no"
LeanBSSNMoL::calculate_constraints    = "yes"

##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Spacetime dissipation
##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
Dissipation::epsdis = 0.2 
Dissipation::order = 5 
Dissipation::vars  = " 
  ADMBase::lapse
  ADMBase::shift
  LeanBSSNMoL::conf_fac
  LeanBSSNMoL::hmetric
  LeanBSSNMoL::gammat
  LeanBSSNMoL::trk
  LeanBSSNMoL::hcurv
"

#LeanBSSNMoL::apply_KO_dissipation = "yes"
#
#LeanBSSNMoL::dissipation_order = 5
#
#LeanBSSNMoL::eps_lapse0    = 0.2
#LeanBSSNMoL::eps_shift0    = 0.2
#LeanBSSNMoL::eps_conf_fac0 = 0.2
#LeanBSSNMoL::eps_hmetric0  = 0.2
#LeanBSSNMoL::eps_gammat0   = 0.2
#LeanBSSNMoL::eps_tracek0   = 0.2
#LeanBSSNMoL::eps_hcurv0    = 0.2

# #------------------------------------------------------------------------------
# #------------------------------------------------------------------------------
# Axidilaton Evolution
# #------------------------------------------------------------------------------
# #------------------------------------------------------------------------------
AxiDil_Base::evolution_method         = "AxiDil_Evol"
AxiDil_Base::RL                       = 1

AxiDil_Base::dil_coupling             = "exponential"
AxiDil_Base::aGB                      = 0.1  # aGB=as={0.001, 0.01, 0.1}
AxiDil_Base::dil_lambda               = 1.0 
AxiDil_Base::dil_potential            = "none"

AxiDil_Base::AD_coupling              = "exponential"
AxiDil_Base::AD_lambda                = 1.0 

AxiDil_Base::axi_coupling             = "linear"
AxiDil_Base::aCS                      = 0.1  # aCS=as={0.001, 0.01, 0.1}
AxiDil_Base::axi_potential            = "none"

AxiDil_Evol::impose_WW_floor          = yes
AxiDil_Evol::WW_floor                 = 1.0d-04
AxiDil_Evol::eps_r                    = 1.0d-06
AxiDil_Evol::derivs_order             = 4
AxiDil_Evol::use_advection_stencils   = "yes"

##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Axidilaton dissipation
##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
AxiDil_Evol::apply_KO_dissipation = "yes"

AxiDil_Evol::dissipation_order = 5

AxiDil_Evol::eps_phi0 = 0.001
AxiDil_Evol::eps_kphi0 = 0.001
AxiDil_Evol::eps_theta0 = 0.001
AxiDil_Evol::eps_ktheta0 = 0.001

##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Integration method
#------------------------------------------------------------------------------
##------------------------------------------------------------------------------
MoL::ODE_Method                 = "RK4"
MoL::MoL_Intermediate_Steps     = 4
MoL::MoL_Num_Scratch_Levels     = 1
Carpet::num_integrator_substeps = 4

##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Spherical surfaces
# #------------------------------------------------------------------------------
##------------------------------------------------------------------------------

SphericalSurface::nsurfaces = 5
SphericalSurface::maxntheta = 66
SphericalSurface::maxnphi   = 124
SphericalSurface::verbose   = no

# Surfaces 0 and 1 are used by PunctureTracker

# Horizon 1
SphericalSurface::ntheta            [2] = 41
SphericalSurface::nphi              [2] = 80
SphericalSurface::nghoststheta      [2] = 2
SphericalSurface::nghostsphi        [2] = 2
CarpetMask::excluded_surface        [0] = 2
CarpetMask::excluded_surface_factor [0] = 1.0

# Horizon 2
SphericalSurface::ntheta            [3] = 41
SphericalSurface::nphi              [3] = 80
SphericalSurface::nghoststheta      [3] = 2
SphericalSurface::nghostsphi        [3] = 2
CarpetMask::excluded_surface        [1] = 3
CarpetMask::excluded_surface_factor [1] = 1.0

# Common horizon
SphericalSurface::ntheta            [4] = 41
SphericalSurface::nphi              [4] = 80
SphericalSurface::nghoststheta      [4] = 2
SphericalSurface::nghostsphi        [4] = 2
CarpetMask::excluded_surface        [2] = 4
CarpetMask::excluded_surface_factor [2] = 1.0

CarpetMask::verbose = no

##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Puncture tracking
# #------------------------------------------------------------------------------
##------------------------------------------------------------------------------

CarpetTracker::surface[0]                       = 0
CarpetTracker::surface[1]                       = 1

PunctureTracker::track                      [0] = yes
PunctureTracker::initial_x                  [0] = 3
PunctureTracker::which_surface_to_store_info[0] = 0
PunctureTracker::track                      [1] = yes
PunctureTracker::initial_x                  [1] = -3
PunctureTracker::which_surface_to_store_info[1] = 1

PunctureTracker::verbose                        = no

##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Wave extraction
#------------------------------------------------------------------------------
##------------------------------------------------------------------------------

NPScalars::NP_order     = 4

Multipole::nradii       = 7
Multipole::radius[0]    = 10
Multipole::radius[0]    = 20
Multipole::radius[1]    = 50
Multipole::radius[1]    = 80
Multipole::radius[2]    =100
Multipole::radius[3]    =150
Multipole::radius[4]    =200
Multipole::nphi         = 240
Multipole::ntheta       = 120
Multipole::integration_method = Simpson
Multipole::variables    = "
  NPScalars::psi4re{sw=-2 cmplx='NPScalars::psi4im' name='NP_Psi4'}
  AxiDil_Base::Phi_gf{sw=0 name='Phi'}
  AxiDil_Evol::rhoDil_gf{sw=0 name='RhoDil'}
  AxiDil_Evol::jrDil_gf{sw=0 name='JrDil'}
  AxiDil_Base::Theta_gf{sw=0 name='Theta'}
  AxiDil_Evol::rhoAxi_gf{sw=0 name='RhoAxi'}
  AxiDil_Evol::jrAxi_gf{sw=0 name='JrAxi'}
"

Multipole::l_max        = 6
Multipole::out_every    = 64
Multipole::output_hdf5  = no
Multipole::output_ascii = yes


##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Horizons
# #------------------------------------------------------------------------------
##------------------------------------------------------------------------------

# AHFinderDirect::verbose_level                           = "algorithm highlights"
AHFinderDirect::verbose_level                            = "physics details"
AHFinderDirect::output_BH_diagnostics                    = "true"
AHFinderDirect::run_at_CCTK_POST_RECOVER_VARIABLES       = no

AHFinderDirect::N_horizons                               = 3
AHFinderDirect::find_every                               = 64

AHFinderDirect::output_h_every                           = 0
AHFinderDirect::max_Newton_iterations__initial           = 50
AHFinderDirect::max_Newton_iterations__subsequent        = 50
AHFinderDirect::max_allowable_Theta_growth_iterations    = 10
AHFinderDirect::max_allowable_Theta_nonshrink_iterations = 10
AHFinderDirect::geometry_interpolator_name               = "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars               = "order=4"
AHFinderDirect::surface_interpolator_name                = "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars                = "order=4"

AHFinderDirect::move_origins                             = yes
AHFinderDirect::reshape_while_moving                     = yes
AHFinderDirect::predict_origin_movement                  = yes

AHFinderDirect::origin_x                             [1] = 3
AHFinderDirect::initial_guess__coord_sphere__x_center[1] = 3
AHFinderDirect::initial_guess__coord_sphere__radius  [1] = 0.25
AHFinderDirect::which_surface_to_store_info          [1] = 2
AHFinderDirect::set_mask_for_individual_horizon      [1] = no
AHFinderDirect::reset_horizon_after_not_finding      [1] = no
AHFinderDirect::track_origin_from_grid_scalar        [1] = yes
AHFinderDirect::track_origin_source_x                [1] = "PunctureTracker::pt_loc_x[0]"
AHFinderDirect::track_origin_source_y                [1] = "PunctureTracker::pt_loc_y[0]"
AHFinderDirect::track_origin_source_z                [1] = "PunctureTracker::pt_loc_z[0]"
AHFinderDirect::max_allowable_horizon_radius         [1] = 2

AHFinderDirect::origin_x                             [2] = -3
AHFinderDirect::initial_guess__coord_sphere__x_center[2] = -3
AHFinderDirect::initial_guess__coord_sphere__radius  [2] = 0.25
AHFinderDirect::which_surface_to_store_info          [2] = 3
AHFinderDirect::set_mask_for_individual_horizon      [2] = no
AHFinderDirect::reset_horizon_after_not_finding      [2] = no
AHFinderDirect::track_origin_from_grid_scalar        [2] = yes
AHFinderDirect::track_origin_source_x                [2] = "PunctureTracker::pt_loc_x[1]"
AHFinderDirect::track_origin_source_y                [2] = "PunctureTracker::pt_loc_y[1]"
AHFinderDirect::track_origin_source_z                [2] = "PunctureTracker::pt_loc_z[1]"
AHFinderDirect::max_allowable_horizon_radius         [2] = 2

AHFinderDirect::origin_x                             [3] = 0
AHFinderDirect::find_after_individual                [3] = 50.0
AHFinderDirect::initial_guess__coord_sphere__x_center[3] = 0
AHFinderDirect::initial_guess__coord_sphere__radius  [3] = 1.0
AHFinderDirect::which_surface_to_store_info          [3] = 4
AHFinderDirect::set_mask_for_individual_horizon      [3] = no
AHFinderDirect::max_allowable_horizon_radius         [3] = 4


##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Isolated Horizons
# #-------------------------------------------------------------------------------
##------------------------------------------------------------------------------

QuasiLocalMeasures::verbose                = yes
QuasiLocalMeasures::veryverbose            = no
QuasiLocalMeasures::interpolator           = "Lagrange polynomial interpolation"
QuasiLocalMeasures::interpolator_options   = "order=4"
QuasiLocalMeasures::spatial_order          = 4
QuasiLocalMeasures::num_surfaces           = 3
QuasiLocalMeasures::surface_index      [0] = 2
QuasiLocalMeasures::surface_index      [1] = 3
QuasiLocalMeasures::surface_index      [2] = 4


##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Check for NaNs
#-------------------------------------------------------------------------------
##------------------------------------------------------------------------------

Carpet::poison_new_timelevels = no
CarpetLib::poison_new_memory  = no
Carpet::check_for_poison      = no

NaNChecker::check_every     = 512
NanChecker::check_after     = 0
NaNChecker::report_max      = 10
NaNChecker::verbose         = "all"
NaNChecker::action_if_found = "terminate"
NaNChecker::out_NaNmask     = yes
NaNChecker::check_vars      = "
  LeanBSSNMoL::conf_fac
  AxiDil_Base::Phi_gf
  AxiDil_Base::Theta_gf
"


##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Timers
#-------------------------------------------------------------------------------
##------------------------------------------------------------------------------

Cactus::cctk_timer_output               = "full"
TimerReport::out_every                  = 5120
TimerReport::n_top_timers               = 40
TimerReport::output_all_timers_together = yes
TimerReport::output_all_timers_readable = yes
TimerReport::output_schedule_timers     = no


##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# I/O thorns
#-------------------------------------------------------------------------------
##------------------------------------------------------------------------------

Cactus::cctk_run_title       = $parfile
IO::out_dir                  = $parfile

IOScalar::one_file_per_group = yes
IOASCII::one_file_per_group  = yes
IOHDF5::one_file_per_group   = no
IOHDF5::use_checksums        = no

IOBasic::outInfo_every       = 64
IOBasic::outInfo_reductions  = "minimum maximum"
IOBasic::outInfo_vars        = "
  Carpet::physical_time_per_hour
  LeanBSSNMoL::ham
  LeanBSSNMoL::mcx
  AxiDil_Base::Phi_gf
  AxiDil_Base::Theta_gf
  #SystemStatistics::maxrss_mb
"

# for scalar reductions of 3D grid functions
IOScalar::outScalar_every               = 64
IOScalar::all_reductions_in_one_file    = "yes"
IOScalar::outScalar_reductions          = "minimum maximum sum average norm1 norm2 norm_inf"
IOScalar::outScalar_vars                = "
  ADMBase::lapse
  #ADMBase::shift
  #LeanBSSNMoL::gammat
  LeanBSSNMoL::conf_fac
  #LeanBSSNMoL::hmetric
  #LeanBSSNMoL::trk
  #LeanBSSNMoL::hcurv
  AxiDil_Base::Phi_gf
  AxiDil_Base::Theta_gf
  LeanBSSNMoL::ham
  LeanBSSNMoL::mom
"

# IOASCII output
IOASCII::output_symmetry_points = no
IOASCII::out3D_ghosts           = no

# output just at one point (0D)
IOASCII::out0D_every = 64
IOASCII::out0D_vars  = "
  Carpet::timing
  QuasiLocalMeasures::qlm_scalars
  AxiDil_Base::Phi_gf
  AxiDil_Base::Theta_gf
  AxiDil_Evol::GBinv_gf
  AxiDil_Evol::CSinv_gf
"

# 1D text output
IOASCII::out1D_every            = 128
IOASCII::out1D_d                = no
IOASCII::out1D_x                = yes
IOASCII::out1D_y                = no
IOASCII::out1D_z                = yes
IOASCII::out1D_vars             = "
  ADMBase::lapse
  #ADMBase::shift
  #LeanBSSNMoL::gammat
  #LeanBSSNMoL::conf_fac
  #LeanBSSNMoL::hmetric
  #LeanBSSNMoL::trk
  #LeanBSSNMoL::hcurv
  LeanBSSNMoL::ham
  LeanBSSNMoL::mom
  AxiDil_Base::Phi_gf
  AxiDil_Evol::AxiDil_TmnDil_Sca_gfs
  AxiDil_Evol::GBinv_gf
  AxiDil_Base::Theta_gf
  AxiDil_Evol::AxiDil_TmnAxi_Sca_gfs
  AxiDil_Evol::CSinv_gf
  #AxiDil_Evol::effective_coupling_constant
  NPScalars::psi4re{sw=-2 cmplx='NPScalars::psi4im' name='NP_Psi4'}
"

# 1D HDF5 output
#IOHDF5::out1D_every            = 256
#IOHDF5::out1D_d                = no
#IOHDF5::out1D_x                = yes
#IOHDF5::out1D_y                = no
#IOHDF5::out1D_z                = no
#IOHDF5::out1D_vars             = "
#  ADMBase::lapse
#"

# 2D HDF5 output
IOHDF5::out2D_every             = 256
IOHDF5::out2D_xy                = yes
IOHDF5::out2D_xz                = yes
IOHDF5::out2D_yz                = no
IOHDF5::out2D_vars              = "
  ADMBase::lapse
  AxiDil_Base::Phi_gf
  AxiDil_Evol::AxiDil_TmnDil_Sca_gfs
  AxiDil_Evol::GBinv_gf
  AxiDil_Base::Theta_gf
  AxiDil_Evol::AxiDil_TmnAxi_Sca_gfs
  AxiDil_Evol::CSinv_gf
  NPScalars::psi4re
"

# # 3D HDF5 output
# IOHDF5::out_every                      = 8192
# IOHDF5::out_vars                       = "
#   ADMBase::lapse
# "

Carpet::verbose                    = no
Carpet::veryverbose                = no
Carpet::schedule_barriers          = no
Carpet::storage_verbose            = no
CarpetLib::output_bboxes           = no

Cactus::cctk_full_warnings         = yes
Cactus::highlight_warning_messages = no


##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Checkpointing and recovery
#-------------------------------------------------------------------------------
##------------------------------------------------------------------------------
CarpetIOHDF5::checkpoint             = yes
IO::checkpoint_every_walltime_hours  = 24
IO::checkpoint_dir                   = "checkpoints"
IO::checkpoint_keep                  = 1
IO::checkpoint_ID                    = no
IO::checkpoint_on_terminate          = yes

IO::recover                          = "autoprobe"
IO::recover_dir                      = "checkpoints"

IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes
CarpetIOHDF5::compression_level             = 9


##------------------------------------------------------------------------------
##------------------------------------------------------------------------------
# Run termination
#-------------------------------------------------------------------------------
##------------------------------------------------------------------------------
TerminationTrigger::max_walltime                 = 24 # hours
TerminationTrigger::on_remaining_walltime        = 30 # minutes
TerminationTrigger::output_remtime_every_minutes = 30

Cactus::terminate       = "time"
Cactus::cctk_final_time = 1000
