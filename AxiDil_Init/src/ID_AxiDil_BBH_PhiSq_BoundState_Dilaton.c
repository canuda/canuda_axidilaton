//* intial data thorn: ID_AD_Dilaton_BBH_PhiSq_BoundState */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
void ID_AD_Dilaton_BBH_PhiSq_BoundState(CCTK_ARGUMENTS);
void
ID_AD_Dilaton_BBH_PhiSq_BoundState (CCTK_ARGUMENTS)
{

  // The individual scalar field profiles are those for single,
  // non-rotating hairy BH solutions in quadratic sGB, (dil_coupling = Phi^2);
  // We use an analytic function fitted to the data of 1711.02080
  // The precise expression is given in Eq. (13) of 2205.06240

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Dilaton_BBH_PhiSq_BoundState initial data ===");

  /*=== define BH parameters ===*/
  /*----------------------------*/
  CCTK_REAL mp, mm;
  mp = m_plus;
  mm = m_minus;

  /* The fitting coefficients */
  /*--------------------------*/
//HW: I am changing notation to be consistent with 
//      Eq. (14) of 2205.06240
  const CCTK_REAL c1 = 3.68375;
  const CCTK_REAL c2 = 4.97242;
  const CCTK_REAL c3 = 229.938;

  /*=== define grid length ===*/
  /*--------------------------*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }

  /*--------------------------*/

/*=== loops over full grid ===*/
/*----------------------------*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);


    /*=== define position parameters ===*/
    /*----------------------------------*/
    CCTK_REAL xp[3], xm[3];
    CCTK_REAL rrp, rrp2;
    CCTK_REAL rrm, rrm2;

    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    xm[0] = x[ind] - pos_minus[0];
    xm[1] = y[ind] - pos_minus[1];
    xm[2] = z[ind] - pos_minus[2];

    rrp = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    if( rrp < eps_r ) rrp = eps_r;
    rrp2 = rrp  * rrp;

    rrm = sqrt( xm[0] * xm[0] + xm[1] * xm[1] + xm[2] * xm[2] );
    if( rrm < eps_r ) rrm = eps_r;
    rrm2 = rrm  * rrm;

    /*----------------------------------*/

    /*=== define scalar fields =========*/
    /*------------------------------------*/
    CCTK_REAL phi_plus, phi_minus;
//HW: taking off below and evaluate sum directly.
//    CCTK_REAL phiBinary;

//HW: I am changing notation to be consistent with 
//      Eq. (14) of 2205.06240
    if( mp == 0 )
    {
      phi_plus = 0;
    } else
    {
      phi_plus = mp * rrp / pow( mp + 2.0*rrp, 2) 
                 * ( c1 + c2 * mp*rrp     / pow( mp + 2.0*rrp, 2)
                        + c3 * mp*mp*rrp2 / pow( mp + 2.0*rrp, 4));
    }

    if( mm == 0 )
    {
      phi_minus = 0;
    } else
    {
      phi_minus = mm * rrm / pow( mm + 2.0*rrm, 2)
                  * ( c1 + c2 * mm*rrm     / pow( mm + 2.0*rrm, 2)
                         + c3 * mm*mm*rrm2 / pow( mm + 2.0*rrm, 4));
    }

    // Approximate binary solution by simply superposing the individual
    // scalar fields solutions
    /*=== write to grid functions ============================*/
    Phi_gf[ind]    = ampsf_m_plus*phi_plus + ampsf_m_minus*phi_minus;
    KPhi_gf[ind]   = 0.0;
    /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/
/*------------------------------*/

  CCTK_INFO("=== End ID_AD_Dilaton_BBH_PhiSq_BoundState initial data ===");

}
