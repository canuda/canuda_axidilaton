#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"

/* -------------------------------------------------------------------*/
void zero_lapse(CCTK_ARGUMENTS);
void
zero_lapse (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  /*=== define BH parameters ===*/
  /*----------------------------*/
  CCTK_REAL mass, mass2, chi, chi2, rBLp;
  mass  = m_plus;
  mass2 = mass * mass;
  chi  = chi_plus;
  chi2 = chi  * chi;
  rBLp  = mass * ( 1 + sqrt( 1 - chi2) );

  /*----------------------------*/

  /*=== define grid length ===*/
  /*--------------------------*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }

  /*--------------------------*/

/*=== loops over full grid ===*/
/*----------------------------*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

    /*=== define position parameters ===*/
    /*----------------------------------*/
    // Define coordinates
    CCTK_REAL xx, yy, zz;
    xx = x[ind] - pos_plus[0];
    yy = y[ind] - pos_plus[1];
    zz = z[ind] - pos_plus[2];

    CCTK_REAL RR, RR2;
    RR2 = xx * xx + yy * yy + zz * zz;
    if( RR2 < pow( eps_r, 2 ) ) 
        RR2 = pow( eps_r, 2 );
    RR  = sqrt( RR2 );
    
    /*=== define metric functions ======*/ 
    /*----------------------------------*/
    CCTK_REAL rBL;
    rBL  = RR * ( 1.0 + 0.25 * rBLp / RR ) * ( 1.0 + 0.25 * rBLp / RR );
    
    /*=== initialize gauge functions ===*/
    /*----------------------------------*/
    if ( RR <= 0.25*rBLp ){
       alp[ind] = 0.0;
    }

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/
/*------------------------------*/
}
