/* intial data thorn: ID_AD_Dilaton_SBH_sGBHair */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
void ID_AD_Dilaton_SBH_sGBHair(CCTK_ARGUMENTS);
void
ID_AD_Dilaton_SBH_sGBHair(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Dilaton_SBH_sGBHair initial data ===");

  /*=== define BH parameters ===*/
  // BH mass
  CCTK_REAL mp;
  mp = m_plus;

  // Dimensionless BH spin chi = a/M
  CCTK_REAL chip, chip2, chip4;
  chip  = chi_plus;
  chip2 = chip * chip;
  chip4 = chip2 * chip2;

  // Horizon radius in Boyer-Lindquist coordinates
  CCTK_REAL rplus;
  rplus = mp * ( 1.0 + sqrt( 1.0 - chip2 ) ); // BHp horizon

  // Quasi-isotropic radial coordinate
  CCTK_REAL rqip;
  CCTK_REAL xp[3];
  CCTK_REAL rrp;
  CCTK_REAL xip, xip2, xip3, xip4;
  CCTK_REAL xip5, xip6, xip7;
  CCTK_REAL CosThp, CosThp2, CosThp4;
  CCTK_REAL phi_plus = 0.0;
  /*==============================*/

  /*=== define grid length ===*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================*/

/*=== loops over full grid ===*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

    /*=== define position parameters ===*/
    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    // Coordinate radius
    rqip = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    if( rqip < eps_r ) rqip = eps_r;

    CosThp  = xp[2] / rqip;
    CosThp2 = CosThp  * CosThp;
    CosThp4 = CosThp2 * CosThp2;

    // Boyer-Lindquist radial coordinate
    rrp = rqip * pow( 1.0 + rplus / (4.0 * rqip),2);
    if( rrp < eps_r ) rrp = eps_r;
    xip = mp / rrp;
    xip2 = xip  * xip;
    xip3 = xip2 * xip;
    xip4 = xip3 * xip;
    xip5 = xip4 * xip;
    xip6 = xip5 * xip;
    xip7 = xip6 * xip;
    /*==============================*/

    /*=== define scalar fields =========*/
    // Dilaton solution provided in Cano et al., Eq. B1 of https://arxiv.org/pdf/2111.04750.pdf
     // Map to conventions used in 2111.04750:
     // aGB = 2 alpha_s/L**2
     // dil_lambda = - 1
     // Checked for consistency also against, e.g., 1405.2133 or 1101.2921
     // Note aGB is defined in AxiDil_Base/param.ccl as the dimensionless ratio
     // aGB = alpha_GB/L**2
     // SBH: L = mp 
     // BBH: L = mp + mm
     // Note: dil_lambda is added to generalize known solutions (e.g., 2111.04750)
     // to different sign choices in f(phi)=exp(dil_lambda*phi) !
     // Note: this solution is valid in the slow-rotating, small-coupling limit
     // up to terms O(aGB^2) and O(chi^6)
     phi_plus =  2.0 * (aGB * pow(RL/mp,2)) * dil_lambda * (
       + ( xip/4.0 + xip2/4.0 + xip3/3.0 )
       - chip2 * (
         + xip/16.0 + xip2/16.0 + xip3/20.0 + xip4/40.0
         + CosThp2 * ( 12.0*xip5/5.0 + 21.0*xip4/20.0 + 7.0*xip3/20.0 )
                 )
       - chip4 * (
         + xip/32.0 + xip2/32.0 + 3.0*xip3/112.0 + xip4/56.0 + xip5/140.0
         - CosThp2 * ( xip3/56.0 + 3.0*xip4/56.0 + 3.0*xip5/35.0 + xip6/14.0 )
         - CosThp4 * ( 45.0*xip7/7.0 + 55.0*xip6/28.0 + 11.0*xip5/28.0 )
                 )
      );

     /*------------------------------------*/

     /*=== write to grid functions ============================*/
     Phi_gf[ind]  = phi_plus;
     KPhi_gf[ind] = 0.0;
     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/

  CCTK_INFO("=== End ID_AD_Dilaton_SBH_sGBHair initial data ===");

}
