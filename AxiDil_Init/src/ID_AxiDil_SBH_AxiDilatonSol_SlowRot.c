//* intial data thorn: ID_AxiDil_SBH_AxiDilatonSol_SlowRot */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
void ID_AxiDil_SBH_AxiDilatonSol_SlowRot(CCTK_ARGUMENTS);
void
ID_AxiDil_SBH_AxiDilatonSol_SlowRot (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AxiDil_SBH_AxiDilatonSol_SlowRot initial data ===");

  /*=== define BH parameters ===*/
  // Mass parameter
  CCTK_REAL mp, mp2, mp3, mp4, mp5, mp6, mp7;
  mp  = m_plus;
  mp2 = mp * mp;
  mp3 = mp2 * mp;
  mp4 = mp3 * mp;
  mp5 = mp4 * mp;
  mp6 = mp5 * mp;
  mp7 = mp6 * mp;

  // Dimensionless spin paramter
  CCTK_REAL chip, chip2, chip3, chip4;
  chip  = chi_plus;
  chip2 = chip  * chip;
  chip3 = chip2 * chip;
  chip4 = chip3 * chip;

  // Horizon radius in Boyer-Lindquist coordinates
  CCTK_REAL rplus;
  rplus = mp * ( 1.0 + sqrt( 1.0 - chip2 ) ); // BHp horizon

  CCTK_REAL xp[3]; // Position
  CCTK_REAL rqip;  // Coordinate radius
  CCTK_REAL rrp, rrp2, rrp3, rrp4, rrp5, rrp6, rrp7; //Boyer-Lindquist radius
  CCTK_REAL CosThp, CosThp2, CosThp3, CosThp4;

  /*=== define scalar fields =========*/
  // The individual scalar field profiles are those for single,
  // slowly-rotating BHs in axi-dilaton gravity described in
  // App. B of Cano et al '21: https://arxiv.org/abs/2111.04750
  CCTK_REAL phi_plus;
  CCTK_REAL theta_plus;

  /*=== define grid length ===*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*--------------------------*/

/*=== loops over full grid ===*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

    /*=== define position parameters ===*/
    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    // Coordinate radius
    rqip = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    if( rqip < eps_r ) rqip = eps_r;

    CosThp  = xp[2] / rqip;
    CosThp2 = CosThp  * CosThp;
    CosThp3 = CosThp2 * CosThp;
    CosThp4 = CosThp3 * CosThp;

    // Boyer-Lindquist radius
    rrp = rqip * pow(1.0 + rplus / (4.0 * rqip), 2); //polar radius from BHp
    if( rrp < eps_r ) rrp = eps_r;
    rrp2 = rrp  * rrp;
    rrp3 = rrp2 * rrp;
    rrp4 = rrp3 * rrp;
    rrp5 = rrp4 * rrp;
    rrp6 = rrp5 * rrp;
    rrp7 = rrp6 * rrp;

    /*----------------------------------*/
    // Dilaton solution provided in Cano et al., Eq. B1 of https://arxiv.org/pdf/2111.04750.pdf
     // Map to conventions used in 2111.04750:
     // aGB = 2 alpha_s/L**2
     // dil_lambda = - 1
     // Checked for consistency also against, e.g., 1405.2133 or 1101.2921
     // Note aGB is defined in AxiDil_Base/param.ccl as the dimensionless ratio
     // aGB = alpha_GB/L**2
     // SBH: L = mp 
     // BBH: L = mp + mm
     // Note: dil_lambda is added to generalize known solutions (e.g., 2111.04750)
     // to different sign choices in f(phi)=exp(dil_lambda*phi) !
     // Note: this solution is valid in the slow-rotating, small-coupling limit
     // up to terms O(aGB^2) and O(chi^6)
     phi_plus =  2.0 * (aGB * pow(RL/mp,2)) * dil_lambda * (
       + ( mp/(4.0*rrp) + mp2/(4.0*rrp2) + mp3/(3.0*rrp3) )
       - chip2 * (
         + mp/(16.0*rrp) + mp2/(16.0*rrp2) + mp3/(20.0*rrp3) + mp4/(40.0*rrp4)
         + CosThp2 * ( 12.0*mp5/(5.0*rrp5) + 21.0*mp4/(20.0*rrp4) + 7.0*mp3/(20.0*rrp3)   )
                 )
       - chip4 * (
         + mp/(32.0*rrp) + mp2/(32.0*rrp2) + 3.0*mp3/(112.0*rrp3) + mp4/(56.0*rrp4) + mp5/(140.0*rrp5)
         - CosThp2 * ( mp3/(56.0*rrp3) + 3.0*mp4/(56.0*rrp4) + 3.0*mp5/(35.0*rrp5) + mp6/(14.0*rrp6) )
         - CosThp4 * ( 45.0*mp7/(7.0*rrp7) + 55.0*mp6/(28.0*rrp6) + 11.0*mp5/(28.0*rrp5) )
                 )
      );

     // Axion solution provided in Cano et al., Eq. B1 of https://arxiv.org/pdf/2111.04750.pdf
     // Map to conventions used in 2111.04750:
     // aCS = -2 alpha_s/L**2
     // Checked for consistency also against, e.g., 1206.6130
     // Note aCS is defined in AxiDil_Base/param.ccl as the dimensionless ratio
     // aCS = alpha_CS/L**2
     // SBH: L = mp 
     // BBH: L = mp + mm
     // Note: this solution is valid in the slow-rotating, small-coupling limit
     // up to terms O(aCS^2) and O(chi^5)
     theta_plus = 2.0 * (aCS * pow(RL/mp,2)) * (
      + chip * CosThp * ( 5.0*mp2/(16.0*rrp2) + 5.0*mp3/(8.0*rrp3) + 9.0*mp4/(8.0*rrp4)      )
      - chip3 * (
        + CosThp  * ( mp2/(32.0*rrp2) + mp3/(16.0*rrp3) + 3.0*mp4/(40.0*rrp4) + mp5/(20.0*rrp5) )
        + CosThp3 * ( 3.0*mp4/(8.0*rrp4) + 3.0*mp5/(2.0*rrp5) + 25.0*mp6/(6.0*rrp6)     )
	)
      );
    /*=== write to grid functions ============================*/
    Phi_gf[ind]  = phi_plus;
    KPhi_gf[ind] = 0.0;
    Theta_gf[ind] = theta_plus;
    KTheta_gf[ind] = 0.0;
    /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/

  CCTK_INFO("=== End ID_AxiDil_SBH_AxiDilatonSol_SlowRot initial data ===");

}
