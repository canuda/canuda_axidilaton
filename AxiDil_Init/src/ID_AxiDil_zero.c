/* intial data: ID_AxiDil_zero */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
/* --- Set Dilaton to zero -------------------------------------------*/
/* -------------------------------------------------------------------*/
void ID_AD_Dilaton_zero(CCTK_ARGUMENTS);
void
ID_AD_Dilaton_zero (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Dilaton_zero initial data ===");

  /*=== define parameters ====================================*/
  /*==========================================================*/

  /*=== define grid length ===================================*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================================================*/

  /*=== loops over full grid =================================*/
  //#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

     /*=== initialize grid functions as zero ==================*/
     Phi_gf[ind]  = 0.0;
     KPhi_gf[ind] = 0.0;
     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
  /*============================================================*/

  CCTK_INFO("=== End ID_AD_Dilaton_zero initial data ===");

}
/* -------------------------------------------------------------------*/

/* -------------------------------------------------------------------*/
/* --- Set Axion to zero ---------------------------------------------*/
/* -------------------------------------------------------------------*/
void ID_AD_Axion_zero(CCTK_ARGUMENTS);
void
ID_AD_Axion_zero (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Axion_zero initial data ===");

  /*=== define parameters ====================================*/
  /*==========================================================*/

  /*=== define grid length ===================================*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================================================*/

  /*=== loops over full grid =================================*/
  //#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

     /*=== initialize grid functions as zero ==================*/
     Theta_gf[ind]  = 0.0;
     KTheta_gf[ind] = 0.0;
     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
  /*============================================================*/

  CCTK_INFO("=== End ID_AD_Axion_zero initial data ===");

}
/* -------------------------------------------------------------------*/

