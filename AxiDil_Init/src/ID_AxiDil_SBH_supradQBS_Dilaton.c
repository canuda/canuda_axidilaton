/* intial data thorn: ID_AD_Dilaton_SBH_supradQBS */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
void ID_AD_Dilaton_SBH_supradQBS(CCTK_ARGUMENTS);
void
ID_AD_Dilaton_SBH_supradQBS(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Dilaton_SBH_supradQBS initial data ===");

  /*=== define parameters ====================================*/
  // Scalar field mass parameter
  CCTK_REAL dil_mu2, dil_mu4;
  dil_mu2 = dil_mu  * dil_mu;
  dil_mu4 = dil_mu2 * dil_mu2;

  // Dimensionless spin parameter chip
  CCTK_REAL chip, chip2;
  chip  = chi_plus;
  chip2 = chip*chip;

  // horizon radius rescaled by mass
  // HW: TODO: CHECK!!! Should be rBLp = mp * (1+BB)
  CCTK_REAL BB, rBLp, rBLm;
  BB   = sqrt( 1.0 - chip2 );
  rBLp = 1.0 + BB;
  rBLm = 1.0 - BB;

  /*==========================================================*/

  /*=== define grid length ===================================*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }

  /*==========================================================*/

/*=== loops over full grid ===================================*/
/*------------------------------------------------------------*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

     /*=== initialize grid functions as zero ==================*/
     Phi_gf[ind]  = 0.0;
     KPhi_gf[ind] = 0.0;
     /*========================================================*/

     /*=== initialize local functions as zero =================*/
     // Local variables for dilaton
     CCTK_REAL psi_re, psi_im, psit_re, psit_im;
     psi_re  = 0.0;
     psi_im  = 0.0;
     psit_re = 0.0;
     psit_im = 0.0;

     // radial and theta functions
     CCTK_REAL RR11_dil, RI11_dil, SR11_dil, SI11_dil;
     RR11_dil = 0.0;
     RI11_dil = 0.0;
     SR11_dil = 0.0;
     SI11_dil = 0.0;
     /*========================================================*/

     /*========================================================*/

     /*=== define radius, r_{BL} and angles ===================*/
     // positions
     CCTK_REAL xp[3];
     xp[0] = x[ind] - pos_plus[0]; 
     xp[1] = y[ind] - pos_plus[1];
     xp[2] = z[ind] - pos_plus[2];

     // coordinate radius and polar radial coordinate
     CCTK_REAL rr, rr2;
     rr = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
     if( rr < eps_r ) rr = eps_r;
     rr2 = rr * rr;

     CCTK_REAL rho, rho2;
     rho = sqrt( xp[0]*xp[0] + xp[1]*xp[1] );
     if( rho < eps_r ) rho = eps_r;
     rho2 = rho * rho;

     // Boyer-Lindquist radial coordinate
     CCTK_REAL rBL;
     rBL  = rr * ( 1.0 + 0.25 * rBLp / rr ) * ( 1.0 + 0.25 * rBLp / rr );
     if( rBL < eps_r ) rBL = eps_r;

     // differences rBL - rBL_{\pm}
     CCTK_REAL Rp, Rm;

     Rp = rBL - rBLp;
     if( abs( Rp ) < eps_r ) Rp = eps_r;

     Rm = rBL - rBLm;
     if( abs(Rm) < eps_r ) Rm = eps_r;

     // angles
     CCTK_REAL CT, CT2, CP;
     CCTK_REAL SP;

     CT  = xp[2] / rr;
     CT2 = CT*CT;
     CP  = xp[0] / rho;

     SP  = xp[1] / rho;
     /*========================================================*/

     /*=== define coefficients, eqs (34) in Dolan '07 =========*/
     // frequency vars
     CCTK_REAL wcrit;
     CCTK_REAL wR_dil2, wR_dil3, wR_dil4;
     CCTK_REAL wI_dil2, wI_dil3, wI_dil4;
     wcrit = chip / ( 2*rBLp );
     wR_dil2   = wR_dil  * wR_dil;
     wR_dil3   = wR_dil2 * wR_dil;
     wR_dil4   = wR_dil2 * wR_dil2;
     wI_dil2   = wI_dil  * wI_dil;
     wI_dil3   = wI_dil2 * wI_dil;
     wI_dil4   = wI_dil2 * wI_dil2;


     CCTK_REAL sigmaR_dil, sigmaI_dil;
     sigmaR_dil = ( BB + 1.0 ) * ( wR_dil - wcrit ) / BB;
     sigmaI_dil = ( BB + 1.0 ) * wI_dil		    / BB; 


     CCTK_REAL qR_dil, qI_dil;
     // NOTE: this uses a series expansion around $\omega_{I} \sim 0$ 
     if( wR_dil2 < dil_mu2 )
     {	
       qR_dil = - ( dil_mu4 + wR_dil4 + 0.5 * dil_mu2 * ( wI_dil2 - 4.0 * wR_dil2 ) ) / sqrt( pow( dil_mu2 - wR_dil2, 3 ) );
       qI_dil =   wR_dil * wI_dil / sqrt( dil_mu2 - wR_dil2 );

     }
     else if( dil_mu2 < wR_dil2 )
     {
       qR_dil = wR_dil * wI_dil / sqrt( wR_dil2 - dil_mu2 );
       qI_dil = - ( dil_mu4 + wR_dil4 + 0.5 * dil_mu2 * ( wI_dil2 - 4.0 * wR_dil2 ) ) / sqrt( pow( wR_dil2 - dil_mu2, 3 ) );

     }
     else if( dil_mu2 == wR_dil2 && wI_dil > 0 )
     {
       qR_dil = - sqrt( wI_dil / dil_mu ) * ( dil_mu + 0.25*wI_dil );
       qI_dil =   sqrt( wI_dil / dil_mu ) * ( dil_mu - 0.25*wI_dil );

     }
     else if( dil_mu2 == wR_dil2 && wI_dil < 0 )
     {
       qR_dil = sqrt( abs(wI_dil) / dil_mu ) * ( dil_mu - 0.25*wI_dil );
       qI_dil = sqrt( abs(wI_dil) / dil_mu ) * ( dil_mu + 0.25*wI_dil );

     }

     CCTK_REAL qR_dil2, qI_dil2, q_dil2, q_dil4;
     qR_dil2 = qR_dil * qR_dil;
     qI_dil2 = qI_dil * qI_dil;
     q_dil2  = qR_dil2 + qI_dil2;
     q_dil4  = q_dil2 * q_dil2;


     CCTK_REAL chiR_dil, chiI_dil;
     chiR_dil = - 4.0*qI_dil * wR_dil * wI_dil / q_dil2 + qR_dil * ( dil_mu2 + 2.0*( wI_dil2 - wR_dil2 ) ) / q_dil2;
     chiI_dil = - 4.0*qR_dil * wR_dil * wI_dil / q_dil2 - qI_dil * ( dil_mu2 + 2.0*( wI_dil2 - wR_dil2 ) ) / q_dil2;

     /*========================================================*/


     /*=== X1R, X1I with X1 = Rp^{-i sigma } ==================*/
     CCTK_REAL X1R_dil, X1I_dil;

     if( Rp > 0.0 )
     {
       X1R_dil =   pow( Rp, sigmaI_dil ) * cos( sigmaR_dil * log( Rp ) );
       X1I_dil = - pow( Rp, sigmaI_dil ) * sin( sigmaR_dil * log( Rp ) ); 
     }
     else if( Rp < 0.0 )
     {
       X1R_dil = pow( abs(Rp), sigmaI_dil ) * exp( Pi*sigmaR_dil ) * cos( Pi*sigmaI_dil - sigmaR_dil * log( abs(Rp) ) );
       X1I_dil = pow( abs(Rp), sigmaI_dil ) * exp( Pi*sigmaR_dil ) * sin( Pi*sigmaI_dil - sigmaR_dil * log( abs(Rp) ) );
     }
     else
     {
       X1R_dil = 0.0;
       X1I_dil = 0.0;
     }

     /*========================================================*/

     /*=== X2R, X2I with X2 = Rm^{i sigma + chi -1} ===========*/
     CCTK_REAL X2R_dil, X2I_dil;
     CCTK_REAL fac_dil;
     fac_dil = chiR_dil - sigmaI_dil - 1.0;

     if( Rm > 0.0 )
     {
       X2R_dil =   pow( Rm, fac_dil ) * cos( (sigmaR_dil + chiI_dil) * log(Rm) ); 
       X2I_dil =   pow( Rm, fac_dil ) * sin( (sigmaR_dil + chiI_dil) * log(Rm) );
     }
     else if( Rm < 0.0 )
     {
       X2R_dil = - pow( abs(Rm), fac_dil ) * exp( - Pi * ( chiI_dil + sigmaR_dil ) ) 
		* cos( Pi * ( chiR_dil - sigmaI_dil ) + ( chiI_dil + sigmaR_dil ) * log(abs(Rm)) );
       X2I_dil =   pow( abs(Rm), fac_dil ) * exp( - Pi * ( chiI_dil + sigmaR_dil ) )
		* sin( Pi * ( sigmaI_dil - chiR_dil ) - ( chiI_dil + sigmaR_dil ) * log(abs(Rm)) );
     }
     else
     {
       X2R_dil = 0.0;
       X2I_dil = 0.0;
     }

     /*========================================================*/

     /*=== X3R, X3I with X3 = exp(q rBL) ======================*/
     CCTK_REAL X3R_dil, X3I_dil;

     X3R_dil = exp( qR_dil*rBL ) * cos( qI_dil*rBL );
     X3I_dil = exp( qR_dil*rBL ) * sin( qI_dil*rBL );

     /*========================================================*/

     /*=== X4R, X4I with X4 = Sum_n An (Rp/Rm)^n ==============*/
     CCTK_REAL X4R_dil, X4I_dil;

	/*--- Lam11, eigenvalue of s=0 spheroidal harmonic ----*/

	CCTK_REAL ff[4];

	ff[0] =   2.0;          // f_{0}
	ff[1] = - 1.0 / 5;      // f_{2}
	ff[2] = - 4.0 / 875;    // f_{4}
	ff[3] = - 8.0 / 65625;  // f_{6}

	CCTK_REAL CCR_dil, CCR_dil2, CCR_dil3, CCI_dil, CCI_dil2;
        CCR_dil = - chip2 * ( dil_mu2 + wI_dil2 - wR_dil2 );
        CCI_dil = 2.0 * chip2 *  wI_dil * wR_dil;

	CCR_dil2 = CCR_dil  * CCR_dil;
	CCR_dil3 = CCR_dil2 * CCR_dil;
 	CCI_dil2 = CCI_dil  * CCI_dil;


	CCTK_REAL LamR11_dil, LamI11_dil;
	LamR11_dil = ff[0] + ff[2] * ( CCR_dil2 - CCI_dil2 ) + CCR_dil3 * ff[3] + CCR_dil * ( ff[1] - 3 * CCI_dil2 * ff[3] );
	LamI11_dil = CCI_dil * ( ff[1] + 2*CCR_dil * ff[2] + ff[3] * ( 3*CCR_dil2 - CCI_dil2 ) );

        /*-----------------------------------------------------*/

        /*--- C0, C1, C2, C3, C4, Eqs. 40 - 44 in Dolan '07 ---*/

	CCTK_REAL C0R_dil, C0I_dil;

	C0R_dil = 1.0 + 2.0 * wI_dil * ( 1.0 + BB ) / BB;
	C0I_dil = ( chip - 2.0 * wR_dil * ( 1.0 + BB ) ) / BB;

	/*-----------------------------------------------------*/
	CCTK_REAL C1R_dil, C1I_dil;

	C1R_dil = - 4.0 + 2*qR_dil * ( 1.0 + 2.0*BB ) - 4*wI_dil * ( 1.0 + 1.0/BB )
              + 2*qR_dil * ( wI_dil2 - wR_dil2 ) / q_dil2 - 4*qI_dil*wI_dil*wR_dil / q_dil2;

	C1I_dil =   2*qI_dil * ( 1.0 + 2.0*BB ) + 4*wR_dil * ( 1.0 + 1.0/BB ) - 2*chip / BB
              + 2*qI_dil * ( wR_dil2 - wI_dil2 ) / q_dil2 - 4*qR_dil*wI_dil*wR_dil / q_dil2;

	/*-----------------------------------------------------*/
	CCTK_REAL C2R_dil, C2I_dil;

	C2R_dil =   3.0 - 2*qR_dil + 2*wI_dil * ( 1.0 + 1.0/BB )
              + 2.0*qR_dil * ( wR_dil2 - wI_dil2 ) / q_dil2 + 4*qI_dil*wI_dil*wR_dil / q_dil2;

	C2I_dil = - 2.0*qI_dil - 2*wR_dil * ( 1.0 + 1.0/BB ) + chip/BB
              + 2.0*qI_dil * ( wI_dil2 - wR_dil2 ) / q_dil2 + 4*qR_dil*wI_dil*wR_dil / q_dil2;

	/*-----------------------------------------------------*/
	CCTK_REAL C3R_dil, C3I_dil;

	C3R_dil = - LamR11_dil - 1.0
              + qR_dil + 6 * ( wR_dil2 - wI_dil2 + qI_dil*wR_dil + qR_dil*wI_dil ) - 2*qI_dil*chip - 2*wI_dil
              + q_dil2 * ( 2.0 - chip2 ) + 2*qR_dil2 * ( chip2 - 2.0 )
              + 2*BB * ( wR_dil2 - wI_dil2 ) + 2*BB * ( q_dil2 + qR_dil ) + 4*BB * ( qR_dil*wI_dil + qI_dil*wR_dil - qR_dil2 )
              - qI_dil*chip / BB + 4 * ( wR_dil2 - wI_dil2 ) / BB + 2 * ( qR_dil*wI_dil + qI_dil*wR_dil - chip*wR_dil - wI_dil ) / BB
              + qR_dil * ( wI_dil2 - wR_dil2 ) / q_dil2  - 6*wI_dil*wR_dil * ( wI_dil*qI_dil + wR_dil*qR_dil ) / q_dil2
              + 2 * ( qR_dil*wI_dil3 + qI_dil*wR_dil3 - qI_dil*wI_dil*wR_dil ) / q_dil2
              + qI_dil*chip * ( wI_dil2 - wR_dil2 ) / (BB*q_dil2) - 6*wI_dil*wR_dil * ( wI_dil*qI_dil + wR_dil*qR_dil ) / (BB*q_dil2)
              + 2 * ( qR_dil*wI_dil3 + qI_dil*wR_dil3 + qR_dil*chip*wI_dil*wR_dil ) / (BB*q_dil2);

	C3I_dil = - LamI11_dil
              +  qI_dil * ( 1.0 - 4*qR_dil ) + 2*qR_dil*chip * ( 1.0 + qI_dil*chip )
              + 6 * ( qI_dil*wI_dil - qR_dil*wR_dil ) + 2*wR_dil * ( 1.0 + 6*wI_dil )
              + 2*BB*qI_dil * ( 1.0 - 2*qR_dil ) + 4*BB * ( qI_dil*wI_dil - qR_dil*wR_dil + wI_dil*wR_dil )
              + chip * ( qR_dil - 1.0 - 2*wI_dil ) / BB
              + 2 * ( qI_dil*wI_dil - qR_dil*wR_dil ) / BB + 2*wR_dil * ( 1.0 + 4*wI_dil ) / BB
              + qI_dil * ( wR_dil2 - wI_dil2 ) / q_dil2 + 6*wI_dil*wR_dil * ( wR_dil*qI_dil - wI_dil*qR_dil ) / q_dil2
              + 2 * ( qR_dil*wR_dil3 - qI_dil*wI_dil3 - qR_dil*wI_dil*wR_dil ) / q_dil2
              + qR_dil*chip * ( wI_dil2 - wR_dil2 ) / (BB*q_dil2) + 6*wI_dil*wR_dil * ( wR_dil*qI_dil - wI_dil*qR_dil ) / (BB*q_dil2)
              + 2 * ( qR_dil*wR_dil3 - qI_dil*wI_dil3 - qI_dil*chip*wI_dil*wR_dil ) / (BB*q_dil2);

	/*-----------------------------------------------------*/
	CCTK_REAL C4R_dil, C4I_dil;

	C4R_dil = - q_dil2 + 2 * ( qR_dil2 - qR_dil*wI_dil + wI_dil2 - wR_dil2 - qI_dil*wR_dil )
              + chip * ( qI_dil + 2*wR_dil ) / BB
              + 4 * ( wI_dil2 - wR_dil2 ) / BB - 2 * ( qI_dil*wR_dil + qR_dil*wI_dil )/ BB
              - ( wR_dil4 + wI_dil4 ) / q_dil2 - 2 * ( qR_dil*wI_dil3 + qI_dil*wR_dil3 ) / q_dil2
              + 6*wI_dil*wR_dil * ( wI_dil*qI_dil + wR_dil*qR_dil + wI_dil*wR_dil ) / q_dil2
              + qI_dil*chip * ( wR_dil2 - wI_dil2 ) / (BB*q_dil2)
              + 6*wI_dil*wR_dil * ( wI_dil*qI_dil + wR_dil*qR_dil ) / (BB*q_dil2)
              - 2 * ( qR_dil*wI_dil3 + qI_dil*wR_dil3 + qR_dil*chip*wI_dil*wR_dil ) / (BB*q_dil2)
              + 2*qR_dil2 * ( wI_dil4 + wR_dil4 - 6*wI_dil2*wR_dil2 ) / q_dil4
              + 8*qI_dil*qR_dil*wI_dil*wR_dil * ( wR_dil2 - wI_dil2 ) / q_dil4;

	C4I_dil =   2 * ( qI_dil*qR_dil + qR_dil*wR_dil - qI_dil*wI_dil - 2*wI_dil*wR_dil )
              + chip * ( 2*wI_dil - qR_dil ) / BB + 2 * ( qR_dil*wR_dil - qI_dil*wI_dil - 4*wI_dil*wR_dil ) / BB
              + 2*wI_dil3 * ( qI_dil + 2*wR_dil ) / q_dil2 - 2*wR_dil3 * ( qR_dil + 2*wI_dil ) / q_dil2
              + 6*wI_dil*wR_dil * ( wI_dil*qR_dil - wR_dil*qI_dil ) / q_dil2
              + qR_dil*chip * ( wR_dil2 - wI_dil2 ) / (BB*q_dil2)
              + 6*wI_dil*wR_dil * ( wI_dil*qR_dil - wR_dil*qI_dil ) / (BB*q_dil2)
              + 2 * ( qI_dil*wI_dil3 + qI_dil*chip*wI_dil*wR_dil - qR_dil*wR_dil3 ) / (BB*q_dil2)
              - 2*qI_dil*qR_dil * ( wI_dil4 + wR_dil4 ) / q_dil4 + 12*qI_dil*qR_dil*wI_dil2*wR_dil2 / q_dil4
              + 8*qR_dil2*wI_dil*wR_dil * ( wR_dil2 - wI_dil2 ) / q_dil4;

	/*-----------------------------------------------------*/

	/*--- alpha, beta, gamma, Eqs. 37 - 39 in Dolan '07 ---*/
	// alpha
	CCTK_REAL alphR_dil[n_sum], alphI_dil[n_sum], alph_dil2[n_sum];
	for( int m = 0; m < n_sum; ++m )
	{

	  alphR_dil[m] = ( C0R_dil + m ) * ( m + 1 );
          alphI_dil[m] = C0I_dil	 * ( m + 1 );
	  alph_dil2[m] = alphR_dil[m]*alphR_dil[m] + alphI_dil[m]*alphI_dil[m];

	}

	/*-----------------------------------------------------*/
	// beta
	CCTK_REAL betaR_dil[n_sum], betaI_dil[n_sum];
	for( int m = 0; m < n_sum; ++m )
	{

          betaR_dil[m] = C3R_dil + m * ( C1R_dil + 2*(1-m) );
          betaI_dil[m] = C3I_dil + m * C1I_dil;

	}

	/*-----------------------------------------------------*/
	// gamma
    	CCTK_REAL gamR_dil[n_sum], gamI_dil[n_sum];
	for( int m = 0; m < n_sum; ++m )
	{

          gamR_dil[m] = C4R_dil + m * ( C2R_dil + m - 3 );
          gamI_dil[m] = C4I_dil + m * C2I_dil;

	}

	/*-----------------------------------------------------*/

	/*--- AnR, AnI, Eqs. 35,36 in Dolan '07 ---------------*/
	CCTK_REAL AnR_dil[n_sum], AnI_dil[n_sum];

	AnR_dil[0] = A0_dil;
	AnI_dil[0] = 0.0;

	AnR_dil[1] = - AnR_dil[0] * ( alphR_dil[0] * betaR_dil[0] + alphI_dil[0] * betaI_dil[0] ) / alph_dil2[0];
	AnI_dil[1] = - AnR_dil[0] * ( alphR_dil[0] * betaI_dil[0] - alphI_dil[0] * betaR_dil[0] ) / alph_dil2[0];

	for( int m = 2; m < n_sum; ++m)
	{

	   AnR_dil[m] = ( - AnR_dil[m-1] * ( alphR_dil[m-1] * betaR_dil[m-1] + alphI_dil[m-1] * betaI_dil[m-1] )
		      + AnI_dil[m-1] * ( alphR_dil[m-1] * betaI_dil[m-1] - alphI_dil[m-1] * betaR_dil[m-1] )
		      - AnR_dil[m-2] * ( alphR_dil[m-1] * gamR_dil[m-1]  + alphI_dil[m-1] * gamI_dil[m-1]  )
		      + AnI_dil[m-2] * ( alphR_dil[m-1] * gamI_dil[m-1]  - alphI_dil[m-1] * gamR_dil[m-1]  )
		    ) / alph_dil2[m-1];

	   AnI_dil[m] = ( - AnR_dil[m-1] * ( alphR_dil[m-1] * betaI_dil[m-1] - alphI_dil[m-1] * betaR_dil[m-1] )
		      - AnI_dil[m-1] * ( alphR_dil[m-1] * betaR_dil[m-1] + alphI_dil[m-1] * betaI_dil[m-1] )
		      - AnR_dil[m-2] * ( alphR_dil[m-1] * gamI_dil[m-1]  - alphI_dil[m-1] * gamR_dil[m-1]  )
		      - AnI_dil[m-2] * ( alphR_dil[m-1] * gamR_dil[m-1]  + alphI_dil[m-1] * gamI_dil[m-1]  ) 
		    ) / alph_dil2[m-1];

	}

	/*-----------------------------------------------------*/
     // X4R, X4I 
     X4R_dil = 0.0;
     X4I_dil = 0.0;

     for( int m = 0; m < n_sum; ++m )
     {

       X4R_dil = X4R_dil + AnR_dil[m] * pow( Rp / Rm, m );
       X4I_dil = X4I_dil + AnI_dil[m] * pow( Rp / Rm, m );

     }
     /*========================================================*/

     /*=== initialize R_{11} spherical harmonics ( in cartesian coordinates ) ===*/

     RR11_dil =  X1I_dil * ( X2I_dil*X3I_dil*X4I_dil - X2R_dil*X3R_dil*X4I_dil - X2R_dil*X3I_dil*X4R_dil - X2I_dil*X3R_dil*X4R_dil )
           - X1R_dil * ( X2R_dil*X3I_dil*X4I_dil + X2I_dil*X3R_dil*X4I_dil + X2I_dil*X3I_dil*X4R_dil - X2R_dil*X3R_dil*X4R_dil );

     RI11_dil =  X1R_dil * (-X2I_dil*X3I_dil*X4I_dil + X2R_dil*X3R_dil*X4I_dil + X2R_dil*X3I_dil*X4R_dil + X2I_dil*X3R_dil*X4R_dil )
           - X1I_dil * ( X2R_dil*X3I_dil*X4I_dil + X2I_dil*X3R_dil*X4I_dil + X2I_dil*X3I_dil*X4R_dil - X2R_dil*X3R_dil*X4R_dil );

     /*========================================================*/

     /*=== s=0 spheroidal harmonics ===========================*/
     // Note: see Eqs. (2.2) - (2.9) in gr-qc/0511111
	/*-----------------------------------------------------*/
	CCTK_REAL cR_dil, cI_dil;
	if( wR_dil2 < dil_mu2 )
	{
	  cR_dil = chip * wI_dil * wR_dil / sqrt( dil_mu2 - wR_dil2 );
	  cI_dil = chip * ( dil_mu4 + wR_dil4 + 0.5 * dil_mu2 * ( wI_dil2 - 4.0*wR_dil2 ) ) / sqrt( pow( dil_mu2 - wR_dil2, 3 ) );
	}
	else if( dil_mu2 < wR_dil2 )
	{
	  cR_dil = chip * ( dil_mu4 + wR_dil4 + 0.5 * dil_mu2 * ( wI_dil2 - 4.0*wR_dil2 ) ) / sqrt( pow( wR_dil2 - dil_mu2, 3 ) );
	  cI_dil = chip * wI_dil * wR_dil / sqrt( wR_dil2 - dil_mu2 );
	}
     	else if( dil_mu2 == wR_dil2 && wI_dil > 0 )
	{
	  cR_dil =   chip * sqrt( wI_dil / dil_mu ) * ( dil_mu - 0.25*wI_dil );
	  cI_dil =   chip * sqrt( wI_dil / dil_mu ) * ( dil_mu + 0.25*wI_dil );
	}
     	else if( dil_mu2 == wR_dil2 && wI_dil < 0 )
	{
	  cR_dil =   chip * sqrt( abs(wI_dil) / dil_mu ) * ( dil_mu + 0.25*wI_dil );
	  cI_dil = - chip * sqrt( abs(wI_dil) / dil_mu ) * ( dil_mu - 0.25*wI_dil );
	}


	CCTK_REAL cR_dil2, cI_dil2;
	cR_dil2 = cR_dil * cR_dil;
	cI_dil2 = cI_dil * cI_dil;

	/*-----------------------------------------------------*/
	CCTK_REAL T1R_dil, T1I_dil;

        T1R_dil = exp( cR_dil * CT ) * cos( cI_dil * CT );
        T1I_dil = exp( cR_dil * CT ) * sin( cI_dil * CT );

	/*-----------------------------------------------------*/
	//Note,  we redefine alph, beta, gam here!!!
	CCTK_REAL aa_dil[n_sum];
	CCTK_REAL bbR_dil[n_sum], bbI_dil[n_sum];
	CCTK_REAL ggR_dil[n_sum], ggI_dil[n_sum];

	for( int p = 0; p < n_sum; ++p )
	{

          aa_dil[p]  = -2.0 * ( p+1 ) * ( p+2 );
          bbR_dil[p] = 2.0 + cI_dil2 - 4 * cR_dil - cR_dil2
                     + 3 * p - 4 * cR_dil * p + p*p - LamR11_dil;
          bbI_dil[p] = -4 * cI_dil - 2 * cI_dil * cR_dil
                     - 4 * cI_dil * p - LamI11_dil;
          ggR_dil[p] = 2.0 * (p+1) * cR_dil;
          ggI_dil[p] = 2.0 * (p+1) * cI_dil;

	}

	/*-----------------------------------------------------*/
	CCTK_REAL ApR_dil[n_sum], ApI_dil[n_sum];

	ApR_dil[0] = Ap0_dil;
	ApI_dil[0] = 0.0;

	ApR_dil[1] = - ApR_dil[0] * bbR_dil[0] / aa_dil[0];
	ApI_dil[1] = - ApR_dil[0] * bbI_dil[0] / aa_dil[0];

	for( int p = 2; p < n_sum; ++p )
	{

	   ApR_dil[p] = ( - ApR_dil[p-1] * bbR_dil[p-1] + ApI_dil[p-1] * bbI_dil[p-1]
		      - ApR_dil[p-2] * ggR_dil[p-1] + ApI_dil[p-2] * ggI_dil[p-1]
		    ) / aa_dil[p-1];

	   ApI_dil[p] = ( - ApR_dil[p-1] * bbI_dil[p-1] - ApI_dil[p-1] * bbR_dil[p-1]
		      - ApR_dil[p-2] * ggI_dil[p-1] - ApI_dil[p-2] * ggR_dil[p-1]
		    ) / aa_dil[p-1];

	}

	/*-----------------------------------------------------*/
	CCTK_REAL  T2R_dil, T2I_dil;
        T2R_dil = 0.0;
        T2I_dil = 0.0;

	for( int p = 0; p < n_sum; ++p )
	{

          T2R_dil = T2R_dil + ApR_dil[p] * pow( 1 + CT, p );
          T2I_dil = T2I_dil + ApI_dil[p] * pow( 1 + CT, p );

	}

	/*-----------------------------------------------------*/
        // HW: TODO: double-check.
        // In Alex' implementation this is (1-CT)
        // In original (reviewed) implementation this is (1-CT2)
     SR11_dil = sqrt( 1.0 - CT2 ) * ( T1R_dil * T2R_dil - T1I_dil * T2I_dil );
     SI11_dil = sqrt( 1.0 - CT2 ) * ( T1R_dil * T2I_dil + T1I_dil * T2R_dil );

     /*========================================================*/

     /*=== initialize Psi =====================================*/
     // Set SF to zero inside horizon to avoid divergences near origin
     CCTK_REAL rH = ( 1.0 + BB ) / 4.0;

     if( rr < rH )
     {
       psi_re = 0.0;
       psi_im = 0.0;
     } 
     else
     {
       psi_re =  ( RR11_dil * SR11_dil - RI11_dil * SI11_dil ) * CP
               - ( RR11_dil * SI11_dil + RI11_dil * SR11_dil ) * SP;

       psi_im =  ( RR11_dil * SI11_dil + RI11_dil * SR11_dil ) * CP
               + ( RR11_dil * SR11_dil - RI11_dil * SI11_dil ) * SP;
     }

     /*========================================================*/

     /*=== calc momentum Kphi =================================*/
     // implement first part of Kphi
     // Kphi = -1/(2\alpha) ( \p_{t} - \Lie_{\beta} ) \phi
     // ASSUME FOR SIMPLICITY: \alpha=1, \beta^i = 0

     psit_re = - 0.5 * ( wR_dil * psi_im + wI_dil * psi_re );
     //psit_im =   0.5 * ( wR_dil * psi_re - wI_dil * psi_im );

     /*========================================================*/

     /*=== write to grid functions ============================*/

     Phi_gf[ind]  = psi_re;
     KPhi_gf[ind] = psit_re;

     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid =================================*/
/*============================================================*/


  CCTK_INFO("=== End ID_AD_Dilaton_SBH_supradQBS initial data ===");

}
/* -------------------------------------------------------------------*/
