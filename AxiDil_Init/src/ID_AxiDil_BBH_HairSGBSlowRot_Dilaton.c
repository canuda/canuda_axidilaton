/* intial data thorn: ID_AD_Dilaton_BBH_sGBHair */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
void ID_AD_Dilaton_BBH_sGBHair(CCTK_ARGUMENTS);
void
ID_AD_Dilaton_BBH_sGBHair(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Dilaton_BBH_sGBHair initial data ===");

  /*=== define BH parameters ===*/
  // BH masses
  CCTK_REAL mp, mm;
  mp = m_plus;
  mm = m_minus;

  // Dimensionless BH spin chi = a/M
  CCTK_REAL chip, chip2, chip4, chim, chim2, chim4;
  chip  = chi_plus;
  chip2 = chip * chip;
  chip4 = chip2 * chip2;
  chim  = chi_minus;
  chim2 = chim * chim;
  chim4 = chim2 * chim2;

  // Horizon radii in Boyer-Lindquist coordinates
  CCTK_REAL rplus, rminus;
  rplus  = mp * ( 1.0 + sqrt( 1.0 - chip2 ) ); // BHp horizon
  rminus = mm * ( 1.0 + sqrt( 1.0 - chim2 ) ); // BHm horizon

  // Coordinate radius
  CCTK_REAL rqip, rqim;
  CCTK_REAL xp[3], xm[3];
  CCTK_REAL rrp, rrm;
  CCTK_REAL xip, xip2, xip3, xip4;
  CCTK_REAL xip5, xip6, xip7;
  CCTK_REAL xim, xim2, xim3, xim4;
  CCTK_REAL xim5, xim6, xim7;
  CCTK_REAL CosThp, CosThm;
  CCTK_REAL CosThp2, CosThm2;
  CCTK_REAL CosThp4, CosThm4;
  CCTK_REAL phi_plus, phi_minus;
  /*==============================*/

  /*=== define grid length ===*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================*/

/*=== loops over full grid ===*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

    /*=== define position parameters ===*/
    // BH plus
    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    // Coordinate radius
    rqip = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    if( rqip < eps_r ) rqip = eps_r;
    CosThp = xp[2]/rqip;
    CosThp2 = CosThp  * CosThp;
    CosThp4 = CosThp2 * CosThp2;

    // Boyer-Lindquist radial coordinate for BHp
    rrp = rqip * pow(1.0 + rplus / (4.0 * rqip),2);
    if( rrp < eps_r ) rrp = eps_r;

    xip = mp / rrp;
    xip2 = xip  * xip;
    xip3 = xip2 * xip;
    xip4 = xip3 * xip;
    xip5 = xip4 * xip;
    xip6 = xip5 * xip;
    xip7 = xip6 * xip;

    xm[0] = x[ind] - pos_minus[0];
    xm[1] = y[ind] - pos_minus[1];
    xm[2] = z[ind] - pos_minus[2];

    // Coordinate radius
    rqim = sqrt( xm[0] * xm[0] + xm[1] * xm[1] + xm[2] * xm[2] );
    if( rqim < eps_r ) rqim = eps_r;
    CosThm = xm[2]/rqim;
    CosThm2 = CosThm  * CosThm;
    CosThm4 = CosThm2 * CosThm2;

    // Boyer-Lindquist radial coordinate of BHm
    rrm = rqim * pow(1.0 + rminus / (4.0 * rqim),2);
    if( rrm < eps_r ) rrm = eps_r;
    xim = mm / rrm;
    xim2 = xim  * xim;
    xim3 = xim2 * xim;
    xim4 = xim3 * xim;
    xim5 = xim4 * xim;
    xim6 = xim5 * xim;
    xim7 = xim6 * xim;

    /*==============================*/

    /*=== define scalar fields =========*/

    if( mp == 0 )
    {
      phi_plus = 0;
    } else
    {
    // Dilaton solution provided in Cano et al., Eq. B1 of https://arxiv.org/pdf/2111.04750.pdf
     // Map to conventions used in 2111.04750:
     // aGB = 2 alpha_s/L**2
     // dil_lambda = - 1
     // Checked for consistency also against, e.g., 1405.2133 or 1101.2921
     // Note aGB is defined in AxiDil_Base/param.ccl as the dimensionless ratio
     // aGB = alpha_GB/L**2
     // SBH: L = mp 
     // BBH: L = mp + mm
     // Note: dil_lambda is added to generalize known solutions (e.g., 2111.04750)
     // to different sign choices in f(phi)=exp(dil_lambda*phi) !
     // Note: this solution is valid in the slow-rotating, small-coupling limit
     // up to terms O(aGB^2) and O(chi^6)
     phi_plus =  2.0 * (aGB * pow(RL/mp,2)) * dil_lambda * (
       + ( xip/4.0 + xip2/4.0 + xip3/3.0 )
       - chip2 * (
         + xip/16.0 + xip2/16.0 + xip3/20.0 + xip4/40.0
         + CosThp2 * ( 12.0*xip5/5.0 + 21.0*xip4/20.0 + 7.0*xip3/20.0 )
                 )
       - chip4 * (
         + xip/32.0 + xip2/32.0 + 3.0*xip3/112.0 + xip4/56.0 + xip5/140.0
         - CosThp2 * ( xip3/56.0 + 3.0*xip4/56.0 + 3.0*xip5/35.0 + xip6/14.0 )
         - CosThp4 * ( 45.0*xip7/7.0 + 55.0*xip6/28.0 + 11.0*xip5/28.0 )
                 )
      );
    }

    if( mm == 0 )
    {
      phi_minus = 0;
    } else
    {
    // Dilaton solution provided in Cano et al., Eq. B1 of https://arxiv.org/pdf/2111.04750.pdf
     // Map to conventions used in 2111.04750:
     // aGB = 2 alpha_s/L**2
     // dil_lambda = - 1
     // Checked for consistency also against, e.g., 1405.2133 or 1101.2921
     // Note aGB is defined in AxiDil_Base/param.ccl as the dimensionless ratio
     // aGB = alpha_GB/L**2
     // SBH: L = mp 
     // BBH: L = mp + mm
     // Note: dil_lambda is added to generalize known solutions (e.g., 2111.04750)
     // to different sign choices in f(phi)=exp(dil_lambda*phi) !
     // Note: this solution is valid in the slow-rotating, small-coupling limit
     // up to terms O(aGB^2) and O(chi^6)
     phi_minus =  2.0 * (aGB * pow(RL/mm,2)) * dil_lambda * (
       + ( xim/4.0 + xim2/4.0 + xim3/3.0 )
       - chim2 * (
         + xim/16.0 + xim2/16.0 + xim3/20.0 + xim4/40.0
         + CosThm2 * ( 12.0*xim5/5.0 + 21.0*xim4/20.0 + 7.0*xim3/20.0 )
                 )
       - chim4 * (
         + xim/32.0 + xim2/32.0 + 3.0*xim3/112.0 + xim4/56.0 + xim5/140.0
         - CosThm2 * ( xim3/56.0 + 3.0*xim4/56.0 + 3.0*xim5/35.0 + xim6/14.0 )
         - CosThm4 * ( 45.0*xim7/7.0 + 55.0*xim6/28.0 + 11.0*xim5/28.0 )
                 )
      );
    }

    /*------------------------------------*/

     /*=== write to grid functions ============================*/
     // Approximate binary solution by simply superposing the individual
     // scalar fields solutions

     Phi_gf[ind]  = phi_plus + phi_minus;
     KPhi_gf[ind] = 0.0;
     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/
/*------------------------------*/

  CCTK_INFO("=== End ID_AD_Dilaton_BBH_sGBHair initial data ===");

}
