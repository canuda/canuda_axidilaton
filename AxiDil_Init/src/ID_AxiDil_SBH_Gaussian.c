/* intial data thorn: ID_AxiDil_SBH_Gaussian */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"


/* -------------------------------------------------------------------*/
/* Dilaton: Gaussian initial data around single BH */
/* -------------------------------------------------------------------*/
void ID_AD_Dilaton_SBH_Gaussian(CCTK_ARGUMENTS);
void
ID_AD_Dilaton_SBH_Gaussian (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Dilaton_SBH_Gaussian initial data ===");

  /*=== define grid length ===================================*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================================================*/

  /*=== loops over full grid =================================*/
  //#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

     /*=== initialize grid functions as zero ==================*/
     Phi_gf[ind]  = 0.0;
     KPhi_gf[ind] = 0.0;
     /*========================================================*/

     /*=== define local variables =============================*/
     CCTK_REAL phi, Kphi;
     CCTK_REAL CoefAng_dil;
     /*========================================================*/

     /*=== initialize local functions as zero =================*/
      phi  = 0.0;
      Kphi = 0.0;

      CoefAng_dil = 0.0;
     /*========================================================*/

     /*=== define position parameters ===*/
     // Note: the Gaussian shell is anchored around pos=0
     CCTK_REAL xp[3];
     CCTK_REAL rrp, rrp2;

     xp[0] = x[ind];
     xp[1] = y[ind];
     xp[2] = z[ind];

     rrp = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
     if( rrp < eps_r ) rrp = eps_r;
     rrp2 = rrp  * rrp;
     /*========================================================*/

     /*=== initialize spherical harmonics =====================*/
     if( CCTK_EQUALS( AxiDil_GaussType_dil, "SF_Gauss00" ) )
     // Z=Y00
     {
       CoefAng_dil = 1.0 / sqrt( 4.0*Pi );
     }
     else if( CCTK_EQUALS( AxiDil_GaussType_dil, "SF_Gauss10" ) )
     // Z=Y10
     {
       CoefAng_dil = sqrt( 0.75 / Pi ) * xp[2] / rrp;
     }
     else if( CCTK_EQUALS( AxiDil_GaussType_dil, "SF_Gauss11" ) )
     // Z=Y1m1-Y11
     {
       CoefAng_dil = sqrt( 1.5 / Pi ) * xp[0] / rrp;
     }
     else if( CCTK_EQUALS( AxiDil_GaussType_dil, "SF_Gauss22" ) )
     // Z=Y22+Y2m2+Y20
     {
       CoefAng_dil = sqrt( 5.0 / (16*Pi) ) * ( 2.0 + ( sqrt(6.0) - 3 )*xp[0]*xp[0] / rrp2 - ( sqrt(6.0) + 3 )*xp[1]*xp[1] / rrp2 );
     }
     else if( CCTK_EQUALS( AxiDil_GaussType_dil, "SF_Gauss0011" ) )
     // Z = Y00 + Y1m1-Y11
     {
       CoefAng_dil = 1.0 / sqrt( 4.0*Pi ) + sqrt( 1.5 / Pi ) * xp[0] / rrp;
     }
     /*-----------------------------------------------------*/
     else
     CCTK_WARN (0, "invalid dilaton field initial data");
     /*-----------------------------------------------------*/

     phi   = ampSF_dil * CoefAng_dil * exp( -( rrp - r0_dil )*( rrp - r0_dil ) / ( width_dil*width_dil ) );

    /*========================================================*/

    //-------------------------------------------
    // Kphi = - {\cal L}_n \Phi
    //      = (1/alpha) * (\partial_t - {\cal L}_{\beta}) \Phi
    //      ~ (1/alpha) * \beta^{k} \partial_{k} \Phi
    // with lapse (alpha) and shift (beta) given by the background spacetime.
    // Here: for now set beta^{i} = 0, i.e., Kphi = 0
    //-------------------------------------------

    /*=== write to grid functions ============================*/
     Phi_gf[ind]  = phi;
     KPhi_gf[ind] = 0.0;
    /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
  /*=== end of loops over grid =================================*/
  /*============================================================*/

  CCTK_INFO("=== End ID_AD_Dilaton_SBH_Gaussian initial data ===");

}
/* -------------------------------------------------------------------*/

/* -------------------------------------------------------------------*/
/* Axion: Gaussian initial data around single BH */
/* -------------------------------------------------------------------*/
void ID_AD_Axion_SBH_Gaussian(CCTK_ARGUMENTS);
void
ID_AD_Axion_SBH_Gaussian (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Axion_SBH_Gaussian initial data ===");

  /*=== define grid length ===================================*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================================================*/

  /*=== loops over full grid =================================*/
  //#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

     /*=== initialize grid functions as zero ==================*/
     Theta_gf[ind]  = 0.0;
     KTheta_gf[ind] = 0.0;
     /*========================================================*/

     /*=== define local variables =============================*/
     CCTK_REAL theta, Ktheta;
     CCTK_REAL CoefAng_axi;
     /*========================================================*/

     /*=== initialize local functions as zero =================*/
      theta  = 0.0;
      Ktheta = 0.0;

      CoefAng_axi = 0.0;
     /*========================================================*/

     /*=== define position parameters ===*/
     // Note: the Gaussian shell is anchored around pos=0
     CCTK_REAL xp[3];
     CCTK_REAL rrp, rrp2;

     xp[0] = x[ind];
     xp[1] = y[ind];
     xp[2] = z[ind];

     rrp = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
     if( rrp < eps_r ) rrp = eps_r;
     rrp2 = rrp  * rrp;
     /*==========================================================*/

     /*=== initialize spherical harmonics =======================*/
     if( CCTK_EQUALS( AxiDil_GaussType_axi, "SF_Gauss00" ) )
     // Z=Y00
     {
       CoefAng_axi = 1.0 / sqrt( 4.0*Pi );
     }
     else if( CCTK_EQUALS( AxiDil_GaussType_axi, "SF_Gauss10" ) )
     // Z=Y10
     {
       CoefAng_axi = sqrt( 0.75 / Pi ) * xp[2] / rrp;
     }
     else if( CCTK_EQUALS( AxiDil_GaussType_axi, "SF_Gauss11" ) )
     // Z=Y1m1-Y11
     {
       CoefAng_axi = sqrt( 1.5 / Pi ) * xp[0] / rrp;
     }
     else if( CCTK_EQUALS( AxiDil_GaussType_axi, "SF_Gauss22" ) )
     // Z=Y22+Y2m2+Y20
     {
       CoefAng_axi = sqrt( 5.0 / (16*Pi) ) * ( 2.0 + ( sqrt(6.0) - 3 )*xp[0]*xp[0] / rrp2 - ( sqrt(6.0) + 3 )*xp[1]*xp[1] / rrp2 );
     }
     else if( CCTK_EQUALS( AxiDil_GaussType_axi, "SF_Gauss0011" ) )
     // Z = Y00 + Y1m1-Y11
     {
       CoefAng_axi = 1.0 / sqrt( 4.0*Pi ) + sqrt( 1.5 / Pi ) * xp[0] / rrp;
     }
     /*-----------------------------------------------------*/
     else
     CCTK_WARN (0, "invalid axion field initial data");

     theta = ampSF_axi * CoefAng_axi * exp( -( rrp - r0_axi )*( rrp - r0_axi ) / ( width_axi*width_axi ) );

    /*========================================================*/

    //-------------------------------------------
    // Ktheta = - {\cal L}_n \Theta
    //        = (1/alpha) * (\partial_t - {\cal L}_{\beta}) \Theta
    //        ~ (1/alpha) * \beta^{k} \partial_{k} \Theta
    // with lapse (alpha) and shift (beta) given by the background spacetime.
    // Here: for now set beta^{i} = 0, i.e., Ktheta = 0
    //-------------------------------------------

    /*=== write to grid functions ============================*/
     Theta_gf[ind]  = theta;
     KTheta_gf[ind] = 0.0;
    /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
  /*=== end of loops over grid ===============================*/

  CCTK_INFO("=== End ID_AD_Axion_SBH_Gaussian initial data ===");

}
/* -------------------------------------------------------------------*/
