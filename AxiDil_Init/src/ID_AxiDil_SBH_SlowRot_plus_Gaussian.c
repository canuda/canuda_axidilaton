//* intial data thorn: ID_AxiDil_SBH_SlowRot_plus_Gaussian */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
void ID_AxiDil_SBH_SlowRot_plus_Gaussian(CCTK_ARGUMENTS);
void
ID_AxiDil_SBH_SlowRot_plus_Gaussian (CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AxiDil_SBH_SlowRot_plus_Gaussian initial data ===");

  /*=== define BH parameters ===*/
  /*----------------------------*/
  CCTK_REAL mp, mp2, mp3, mp4, mp5;
  mp = m_plus;
  mp2 = mp * mp;
  mp3 = mp2 * mp;
  mp4 = mp2 * mp2;
  mp5 = mp4 * mp;
  CCTK_REAL chip, chip2, chip3, chip4;
  chip = spin_plus/m_plus;
  chip2 = chip * chip;
  chip3 = chip2 * chip;
  chip4 = chip2 * chip2;
  CCTK_REAL rplus;
//HW: this should be sqrt(m2 - spin2) i.e. "-" instead of "+"
//  rplus = mp + sqrt(mp*mp + spin_plus*spin_plus); //BHp horizon
  // Horizon radius in BL coordinates
  rplus = mp + sqrt( mp2 - spin_plus * spin_plus);
  CCTK_REAL xp[3];
  CCTK_REAL rrp, rrp2, rrp3, rrp4, rrp5, rrp6, rrp7;
  CCTK_REAL CosThp, CosThp2, CosThp3, CosThp4;
  CCTK_REAL rqip;

  /*=== define scalar fields =========*/
  /*------------------------------------*/
  // The individual scalar field profiles are those for single,
  // slowly-rotating BHs in the axi-dilaton gravity described in
  // App. B of https://arxiv.org/abs/2111.04750
  CCTK_REAL phi_plus;
  CCTK_REAL theta_plus;

  /*=== define variable for Gaussian ID ====*/
  /*------------------------------------*/
  // Define Gaussian perturbation for l = m = 0, l = 1 m = 0,
  // and l = m = 1 modes to test mode stability and decay rates.
  CCTK_REAL phi_Gauss, theta_Gauss;
  CCTK_REAL CoefAng_dil, CoefAng_axi;

  /*=== define grid length ===*/
  /*--------------------------*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }

  /*--------------------------*/

/*=== loops over full grid ===*/
/*----------------------------*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);
    
    /*=== initialize grid functions as zero ==================*/
     Phi_gf[ind]  = 0.0;
     Theta_gf[ind]  = 0.0;
    /*========================================================*/

  /*=== initialize local functions as zero =================*/
      phi_Gauss  = 0.0;

      theta_Gauss  = 0.0;

      CoefAng_dil = 0.0;
      CoefAng_axi = 0.0;
  /*========================================================*/

    /*=== define position parameters ===*/
    /*----------------------------------*/
    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    rqip = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    CosThp = xp[2]/rqip;
    CosThp2 = CosThp * CosThp;
    CosThp3 = CosThp2 * CosThp;
    CosThp4 = CosThp2 * CosThp2;
    rrp = rqip * pow(1.0 + rplus / (4.0 * rqip),2); //polar radius from BHp
    if( rrp < eps_r ) rrp = eps_r;
    rrp2 = rrp  * rrp;
    rrp3 = rrp2 * rrp;
    rrp4 = rrp3 * rrp;
    rrp5 = rrp4 * rrp;
    rrp6 = rrp5 * rrp;
    rrp7 = rrp6 * rrp;

    /*----------------------------------*/
   // This is - the solution provided in Cano et al., App. B of https://arxiv.org/pdf/2111.04750.pdf
// HW: Where does the overall factor 2 and the overall "-" come from? See B1 of Cano et al.
// HW: if this is correct, please add a comment why/how this is
// different
// HW: the rest looks correct :)
    phi_plus =  -2.0 * aGB * dil_lambda * (
       - ( 1.0/(4.0*mp*rrp) + 1.0/(4.0*rrp2) + mp/(3.0*rrp3) )
       + chip2 * (
        + 1.0/(16.0*mp*rrp) + 1.0/(16.0*rrp2) + mp/(20.0*rrp3) + mp2/(40.0*rrp4)
        + CosThp2 * ( 12.0*mp3/(5.0*rrp5) + 21.0*mp2/(20.0*rrp4) + 7.0*mp/(20.0*rrp3)
                    )
                 )
       + chip4 * (
        + 1.0/(32.0*mp*rrp) + 1.0/(32.0*rrp2) + 3.0*mp/(112.0*rrp3) + mp2/(56.0*rrp4)
        + mp3/(140.0*rrp5)
        - CosThp2 * ( mp/(56.0*rrp3) + 3.0*mp2/(56.0*rrp4) + 3.0*mp3/(35.0*rrp5)
                     + mp4/(14.0*rrp6) )
        - CosThp4 * ( 45.0*mp5/(7.0*rrp7) + 55.0*mp4/(28.0*rrp6) + 11.0*mp3/(28.0*rrp5)
                    )
                 )
      );

   // This is - the solution provided in Cano et al.
// HW: again, there is an overall factor of $-2$. Where does it come
// from? Add a brief comment to explain 
// HW: i took off unnecessary bracket in first term; the rest looks
// correct :)
    theta_plus = -2.0 * aCS * axi_lambda * (
      - chip * CosThp * ( 5.0/(16.0*rrp2) + 5.0*mp/(8.0*rrp3) + 9.0*mp2/(8.0*rrp4) )
      + chip3 * (
        + CosThp * ( 1.0/(32.0*rrp2) + mp/(16.0*rrp3) + 3.0*mp2/(40.0*rrp4)
                    + mp3/(20.0*rrp5) )
        + CosThp3 * ( 3.0*mp2/(8.0*rrp4) + 3.0*mp3/(2.0*rrp5) + 25.0*mp4/(6.0*rrp6)
                    )
                )
     );


    /*=== initialize spherical harmonics =====================*/
//HW: why only the l=1 modes? extend to include l=0 and l=2
//perturbations
	
    // Z = Y00 + Y10 + Y1m1-Y11 

    CoefAng_dil = 1.0 / sqrt( 4.0*Pi ) + sqrt( 0.75 / Pi ) * xp[2] / rrp + sqrt( 1.5 / Pi ) * xp[0] / rrp;

    CoefAng_axi = 1.0 / sqrt( 4.0*Pi ) + sqrt( 0.75 / Pi ) * xp[2] / rrp + sqrt( 1.5 / Pi ) * xp[0] / rrp;    

    /*=================== set Gaussians  =====================*/

     phi_Gauss   = ampSF_dil * CoefAng_dil * exp( -( rrp - r0_dil )*( rrp - r0_dil ) / ( width_dil*width_dil ) );
     theta_Gauss = ampSF_axi * CoefAng_axi * exp( -( rrp - r0_axi )*( rrp - r0_axi ) / ( width_axi*width_axi ) );


    /*=== write to grid functions ============================*/
    Phi_gf[ind]  = phi_plus + phi_Gauss;
    KPhi_gf[ind] = 0.0;
    Theta_gf[ind] = theta_plus + theta_Gauss;
    KTheta_gf[ind] = 0.0;
    /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/
/*------------------------------*/

  CCTK_INFO("=== End ID_AxiDil_SBH_SlowRot_plus_Gaussian initial data ===");

}
