/* intial data thorn: ID_AD_Axion_SBH_DCSHair */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
void ID_AD_Axion_SBH_DCSHair(CCTK_ARGUMENTS);
void
ID_AD_Axion_SBH_DCSHair(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Axion_SBH_DCSHair initial data ===");

  /*=== define BH parameters ===*/
  // BH mass
  CCTK_REAL mp;
  mp = m_plus;

  // Dimensionless BH spin chi = a/M
  CCTK_REAL chip, chip2, chip3;
  chip  = chi_plus;
  chip2 = chip * chip;
  chip3 = chip2 * chip;

  // Horizon radius in Boyer-Lindquist coordinates
  CCTK_REAL rplus;
  rplus = mp * ( 1.0 + sqrt( 1.0 - chip2 ) ); // BHp horizon

  // Quasi-isotropic radial coordinate
  CCTK_REAL rqip;
  CCTK_REAL xp[3];
  CCTK_REAL rrp;
  CCTK_REAL xip, xip2, xip3, xip4, xip5, xip6;
  CCTK_REAL CosThp, CosThp3;
  CCTK_REAL theta_plus = 0.0;
  /*==============================*/

  /*=== define grid length ===*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }
  /*==========================*/

/*=== loops over full grid ===*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

    /*=== define position parameters ===*/
    xp[0] = x[ind] - pos_plus[0];
    xp[1] = y[ind] - pos_plus[1];
    xp[2] = z[ind] - pos_plus[2];

    // Coordinate radius
    rqip = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
    if( rqip < eps_r ) rqip = eps_r;

    CosThp = xp[2]/rqip;
    CosThp3 = CosThp * CosThp * CosThp;

    // Boyer-Lindquist radial coordinate
    rrp = rqip * pow( 1.0 + rplus / (4.0 * rqip),2);
    if( rrp < eps_r ) rrp = eps_r;
    xip = mp / rrp;
    xip2 = xip  * xip;
    xip3 = xip2 * xip;
    xip4 = xip3 * xip;
    xip5 = xip4 * xip;
    xip6 = xip5 * xip;

    /*=== define scalar fields =========*/
     // Axion solution provided in Cano et al., Eq. B1 of https://arxiv.org/pdf/2111.04750.pdf
     // Map to conventions used in 2111.04750:
     // aCS = -2 alpha_s/L**2
     // Checked for consistency also against, e.g., 1206.6130
     // Note aCS is defined in AxiDil_Base/param.ccl as the dimensionless ratio
     // aCS = alpha_CS/L**2
     // SBH: L = mp 
     // BBH: L = mp + mm
     // Note: this solution is valid in the slow-rotating, small-coupling limit
     // up to terms O(aCS^2) and O(chi^5)
     theta_plus = 2.0 * (aCS * pow(RL/mp,2)) * (
      + chip * CosThp * ( 5.0*xip2/16.0 + 5.0*xip3/8.0 + 9.0*xip4/8.0      )
      - chip3 * (
        + CosThp  * ( xip2/32.0 + xip3/16.0 + 3.0*xip4/40.0 + xip5/20.0 )
        + CosThp3 * ( 3.0*xip4/8.0 + 3.0*xip5/2.0 + 25.0*xip6/6.0     )
	)
      );

     /*=== write to grid functions ============================*/
     Theta_gf[ind]  = theta_plus;
     KTheta_gf[ind] = 0.0;
     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid ===*/

  CCTK_INFO("=== End ID_AD_Axion_SBH_DCSHair initial data ===");

}
