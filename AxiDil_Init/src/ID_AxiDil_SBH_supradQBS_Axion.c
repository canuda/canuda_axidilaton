/* intial data thorn: ID_AD_Axion_SBH_supradQBS */
/*======================================================*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include "cctk.h"
#include "cctk_Arguments.h"
#include "cctk_Parameters.h"
#include "ID_AxiDil_utils.h"

/* -------------------------------------------------------------------*/
void ID_AD_Axion_SBH_supradQBS(CCTK_ARGUMENTS);
void
ID_AD_Axion_SBH_supradQBS(CCTK_ARGUMENTS)
{

  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;

  CCTK_INFO("=== Begin ID_AD_Axion_SBH_supradQBS initial data ===");

  /*=== define parameters ====================================*/
  // Scalar field mass parameter
  CCTK_REAL axi_mu2, axi_mu4;
  axi_mu2 = axi_mu  * axi_mu;
  axi_mu4 = axi_mu2 * axi_mu2;

  // Dimensionless spin parameter chip
  CCTK_REAL chip, chip2;
  chip  = chi_plus;
  chip2 = chip*chip;

  // horizon radius rescaled by mass
  // HW: TODO: CHECK!!! Should be rBLp = mp * (1+BB)
  CCTK_REAL BB, rBLp, rBLm;
  BB   = sqrt( 1.0 - chip2 );
  rBLp = 1.0 + BB;
  rBLm = 1.0 - BB;

  /*==========================================================*/

  /*=== define grid length ===================================*/
  CCTK_INT imin[3], imax[3];
  for (int d = 0; d < 3; ++ d)
  {
    imin[d] = 0;
    imax[d] = cctk_lsh[d];
  }

  /*==========================================================*/

/*=== loops over full grid ===================================*/
/*------------------------------------------------------------*/
//#pragma omp parallel for
  for (int k = imin[2]; k < imax[2]; ++k)
  {
   for (int j = imin[1]; j < imax[1]; ++j)
   {
    for (int i = imin[0]; i < imax[0]; ++i)
    {

     const int ind = CCTK_GFINDEX3D (cctkGH, i, j, k);

     /*=== initialize grid functions as zero ==================*/
     Theta_gf[ind]  = 0.0;
     KTheta_gf[ind] = 0.0;
     /*========================================================*/

     /*=== initialize local functions as zero =================*/
     // Local variables for axion
     CCTK_REAL xi_re, xi_im, xit_re, xit_im;
     xi_re  = 0.0;
     xi_im  = 0.0;
     xit_re = 0.0;
     xit_im = 0.0;

     // radial and theta functions
     CCTK_REAL RR11_axi, RI11_axi, SR11_axi, SI11_axi;
     RR11_axi = 0.0;
     RI11_axi = 0.0;
     SR11_axi = 0.0;
     SI11_axi = 0.0;
     /*========================================================*/

     /*========================================================*/

     /*=== define radius, r_{BL} and angles ===================*/
     // positions
     CCTK_REAL xp[3];
     xp[0] = x[ind] - pos_plus[0]; 
     xp[1] = y[ind] - pos_plus[1];
     xp[2] = z[ind] - pos_plus[2];

     // coordinate radius and polar radial coordinate
     CCTK_REAL rr, rr2;
     rr = sqrt( xp[0] * xp[0] + xp[1] * xp[1] + xp[2] * xp[2] );
     if( rr < eps_r ) rr = eps_r;
     rr2 = rr * rr;

     CCTK_REAL rho, rho2;
     rho = sqrt( xp[0]*xp[0] + xp[1]*xp[1] );
     if( rho < eps_r ) rho = eps_r;
     rho2 = rho*rho;

     // Boyer-Lindquist radial coordinate
     CCTK_REAL rBL;
     rBL  = rr * ( 1.0 + 0.25 * rBLp / rr ) * ( 1.0 + 0.25 * rBLp / rr );
     if( rBL < eps_r ) rBL = eps_r;

     // differences rBL - rBL_{\pm}
     CCTK_REAL Rp, Rm;

     Rp = rBL - rBLp;
     if( abs( Rp ) < eps_r ) Rp = eps_r;

     Rm = rBL - rBLm;
     if( abs(Rm) < eps_r ) Rm = eps_r;

     // angles
     CCTK_REAL CT, CT2, CP;
     CCTK_REAL SP;

     CT  = xp[2] / rr;
     CT2 = CT*CT;
     CP  = xp[0] / rho;

     SP  = xp[1] / rho;
     /*========================================================*/

     /*=== define coefficients, eqs (34) in Dolan '07 =========*/
     // frequency vars
     CCTK_REAL wcrit;
     CCTK_REAL wR_axi2, wR_axi3, wR_axi4;
     CCTK_REAL wI_axi2, wI_axi3, wI_axi4;
     wcrit = chip / ( 2*rBLp );
     wR_axi2   = wR_axi  * wR_axi;
     wR_axi3   = wR_axi2 * wR_axi;
     wR_axi4   = wR_axi2 * wR_axi2;
     wI_axi2   = wI_axi  * wI_axi;
     wI_axi3   = wI_axi2 * wI_axi;
     wI_axi4   = wI_axi2 * wI_axi2;


     CCTK_REAL sigmaR_axi, sigmaI_axi;
     sigmaR_axi = ( BB + 1.0 ) * ( wR_axi - wcrit ) / BB;
     sigmaI_axi = ( BB + 1.0 ) * wI_axi		    / BB; 


     CCTK_REAL qR_axi = 0.0;
     CCTK_REAL qI_axi = 0.0;
     // NOTE: this uses a series expansion around $\omega_{I} \sim 0$ 
     if( wR_axi2 < axi_mu2 )
     {	
       qR_axi = - ( axi_mu4 + wR_axi4 + 0.5 * axi_mu2 * ( wI_axi2 - 4.0 * wR_axi2 ) ) / sqrt( pow( axi_mu2 - wR_axi2, 3 ) );
       qI_axi =   wR_axi * wI_axi / sqrt( axi_mu2 - wR_axi2 );

     }
     else if( axi_mu2 < wR_axi2 )
     {
       qR_axi = wR_axi * wI_axi / sqrt( wR_axi2 - axi_mu2 );
       qI_axi = - ( axi_mu4 + wR_axi4 + 0.5 * axi_mu2 * ( wI_axi2 - 4.0 * wR_axi2 ) ) / sqrt( pow( wR_axi2 - axi_mu2, 3 ) );

     }
     else if( axi_mu2 == wR_axi2 && wI_axi > 0 )
     {
       qR_axi = - sqrt( wI_axi / axi_mu ) * ( axi_mu + 0.25*wI_axi );
       qI_axi =   sqrt( wI_axi / axi_mu ) * ( axi_mu - 0.25*wI_axi );

     }
     else if( axi_mu2 == wR_axi2 && wI_axi < 0 )
     {
       qR_axi = sqrt( abs(wI_axi) / axi_mu ) * ( axi_mu - 0.25*wI_axi );
       qI_axi = sqrt( abs(wI_axi) / axi_mu ) * ( axi_mu + 0.25*wI_axi );
     }

     CCTK_REAL qR_axi2, qI_axi2, q_axi2, q_axi4;
     qR_axi2 = qR_axi * qR_axi;
     qI_axi2 = qI_axi * qI_axi;
     q_axi2  = qR_axi2 + qI_axi2;
     q_axi4  = q_axi2 * q_axi2;


     CCTK_REAL chiR_axi, chiI_axi;
     chiR_axi = - 4.0*qI_axi * wR_axi * wI_axi / q_axi2 + qR_axi * ( axi_mu2 + 2.0*( wI_axi2 - wR_axi2 ) ) / q_axi2;
     chiI_axi = - 4.0*qR_axi * wR_axi * wI_axi / q_axi2 - qI_axi * ( axi_mu2 + 2.0*( wI_axi2 - wR_axi2 ) ) / q_axi2;

     /*========================================================*/


     /*=== X1R, X1I with X1 = Rp^{-i sigma } ==================*/
     CCTK_REAL X1R_axi, X1I_axi;

     if( Rp > 0.0 )
     {
       X1R_axi =   pow( Rp, sigmaI_axi ) * cos( sigmaR_axi * log( Rp ) );
       X1I_axi = - pow( Rp, sigmaI_axi ) * sin( sigmaR_axi * log( Rp ) ); 
     }
     else if( Rp < 0.0 )
     {
       X1R_axi = pow( abs(Rp), sigmaI_axi ) * exp( Pi*sigmaR_axi ) * cos( Pi*sigmaI_axi - sigmaR_axi * log( abs(Rp) ) );
       X1I_axi = pow( abs(Rp), sigmaI_axi ) * exp( Pi*sigmaR_axi ) * sin( Pi*sigmaI_axi - sigmaR_axi * log( abs(Rp) ) );
     }
     else
     {
       X1R_axi = 0.0;
       X1I_axi = 0.0;
     }

     /*========================================================*/

     /*=== X2R, X2I with X2 = Rm^{i sigma + chi -1} ===========*/
     CCTK_REAL X2R_axi, X2I_axi;
     CCTK_REAL fac_axi;
     fac_axi = chiR_axi - sigmaI_axi - 1.0;

     if( Rm > 0.0 )
     {
       X2R_axi =   pow( Rm, fac_axi ) * cos( (sigmaR_axi + chiI_axi) * log(Rm) ); 
       X2I_axi =   pow( Rm, fac_axi ) * sin( (sigmaR_axi + chiI_axi) * log(Rm) );
     }
     else if( Rm < 0.0 )
     {
       X2R_axi = - pow( abs(Rm), fac_axi ) * exp( - Pi * ( chiI_axi + sigmaR_axi ) ) 
		* cos( Pi * ( chiR_axi - sigmaI_axi ) + ( chiI_axi + sigmaR_axi ) * log(abs(Rm)) );
       X2I_axi =   pow( abs(Rm), fac_axi ) * exp( - Pi * ( chiI_axi + sigmaR_axi ) )
		* sin( Pi * ( sigmaI_axi - chiR_axi ) - ( chiI_axi + sigmaR_axi ) * log(abs(Rm)) );
     }
     else
     {
       X2R_axi = 0.0;
       X2I_axi = 0.0;
     }

     /*========================================================*/

     /*=== X3R, X3I with X3 = exp(q rBL) ======================*/
     CCTK_REAL X3R_axi, X3I_axi;

     X3R_axi = exp( qR_axi*rBL ) * cos( qI_axi*rBL );
     X3I_axi = exp( qR_axi*rBL ) * sin( qI_axi*rBL );

     /*========================================================*/

     /*=== X4R, X4I with X4 = Sum_n An (Rp/Rm)^n ==============*/
     CCTK_REAL X4R_axi, X4I_axi;

	/*--- Lam11, eigenvalue of s=0 spheroidal harmonic ----*/

	CCTK_REAL ff[4];

	ff[0] =   2.0;          // f_{0}
	ff[1] = - 1.0 / 5;      // f_{2}
	ff[2] = - 4.0 / 875;    // f_{4}
	ff[3] = - 8.0 / 65625;  // f_{6}

	CCTK_REAL CCR_axi, CCR_axi2, CCR_axi3, CCI_axi, CCI_axi2;
        CCR_axi = - chip2 * ( axi_mu2 + wI_axi2 - wR_axi2 );
        CCI_axi = 2.0 * chip2 *  wI_axi * wR_axi;

	CCR_axi2 = CCR_axi  * CCR_axi;
	CCR_axi3 = CCR_axi2 * CCR_axi;
 	CCI_axi2 = CCI_axi  * CCI_axi;


	CCTK_REAL LamR11_axi, LamI11_axi;
	LamR11_axi = ff[0] + ff[2] * ( CCR_axi2 - CCI_axi2 ) + CCR_axi3 * ff[3] + CCR_axi * ( ff[1] - 3 * CCI_axi2 * ff[3] );
	LamI11_axi = CCI_axi * ( ff[1] + 2*CCR_axi * ff[2] + ff[3] * ( 3*CCR_axi2 - CCI_axi2 ) );

        /*-----------------------------------------------------*/

        /*--- C0, C1, C2, C3, C4, Eqs. 40 - 44 in Dolan '07 ---*/

	CCTK_REAL C0R_axi, C0I_axi;

	C0R_axi = 1.0 + 2.0 * wI_axi * ( 1.0 + BB ) / BB;
	C0I_axi = ( chip - 2.0 * wR_axi * ( 1.0 + BB ) ) / BB;

	/*-----------------------------------------------------*/
	CCTK_REAL C1R_axi, C1I_axi;

	C1R_axi = - 4.0 + 2*qR_axi * ( 1.0 + 2.0*BB ) - 4*wI_axi * ( 1.0 + 1.0/BB )
              + 2*qR_axi * ( wI_axi2 - wR_axi2 ) / q_axi2 - 4*qI_axi*wI_axi*wR_axi / q_axi2;

	C1I_axi =   2*qI_axi * ( 1.0 + 2.0*BB ) + 4*wR_axi * ( 1.0 + 1.0/BB ) - 2*chip / BB
              + 2*qI_axi * ( wR_axi2 - wI_axi2 ) / q_axi2 - 4*qR_axi*wI_axi*wR_axi / q_axi2;

	/*-----------------------------------------------------*/
	CCTK_REAL C2R_axi, C2I_axi;

	C2R_axi =   3.0 - 2*qR_axi + 2*wI_axi * ( 1.0 + 1.0/BB )
              + 2.0*qR_axi * ( wR_axi2 - wI_axi2 ) / q_axi2 + 4*qI_axi*wI_axi*wR_axi / q_axi2;

	C2I_axi = - 2.0*qI_axi - 2*wR_axi * ( 1.0 + 1.0/BB ) + chip/BB
              + 2.0*qI_axi * ( wI_axi2 - wR_axi2 ) / q_axi2 + 4*qR_axi*wI_axi*wR_axi / q_axi2;

	/*-----------------------------------------------------*/
	CCTK_REAL C3R_axi, C3I_axi;

	C3R_axi = - LamR11_axi - 1.0
              + qR_axi + 6 * ( wR_axi2 - wI_axi2 + qI_axi*wR_axi + qR_axi*wI_axi ) - 2*qI_axi*chip - 2*wI_axi
              + q_axi2 * ( 2.0 - chip2 ) + 2*qR_axi2 * ( chip2 - 2.0 )
              + 2*BB * ( wR_axi2 - wI_axi2 ) + 2*BB * ( q_axi2 + qR_axi ) + 4*BB * ( qR_axi*wI_axi + qI_axi*wR_axi - qR_axi2 )
              - qI_axi*chip / BB + 4 * ( wR_axi2 - wI_axi2 ) / BB + 2 * ( qR_axi*wI_axi + qI_axi*wR_axi - chip*wR_axi - wI_axi ) / BB
              + qR_axi * ( wI_axi2 - wR_axi2 ) / q_axi2  - 6*wI_axi*wR_axi * ( wI_axi*qI_axi + wR_axi*qR_axi ) / q_axi2
              + 2 * ( qR_axi*wI_axi3 + qI_axi*wR_axi3 - qI_axi*wI_axi*wR_axi ) / q_axi2
              + qI_axi*chip * ( wI_axi2 - wR_axi2 ) / (BB*q_axi2) - 6*wI_axi*wR_axi * ( wI_axi*qI_axi + wR_axi*qR_axi ) / (BB*q_axi2)
              + 2 * ( qR_axi*wI_axi3 + qI_axi*wR_axi3 + qR_axi*chip*wI_axi*wR_axi ) / (BB*q_axi2);

	C3I_axi = - LamI11_axi
              +  qI_axi * ( 1.0 - 4*qR_axi ) + 2*qR_axi*chip * ( 1.0 + qI_axi*chip )
              + 6 * ( qI_axi*wI_axi - qR_axi*wR_axi ) + 2*wR_axi * ( 1.0 + 6*wI_axi )
              + 2*BB*qI_axi * ( 1.0 - 2*qR_axi ) + 4*BB * ( qI_axi*wI_axi - qR_axi*wR_axi + wI_axi*wR_axi )
              + chip * ( qR_axi - 1.0 - 2*wI_axi ) / BB
              + 2 * ( qI_axi*wI_axi - qR_axi*wR_axi ) / BB + 2*wR_axi * ( 1.0 + 4*wI_axi ) / BB
              + qI_axi * ( wR_axi2 - wI_axi2 ) / q_axi2 + 6*wI_axi*wR_axi * ( wR_axi*qI_axi - wI_axi*qR_axi ) / q_axi2
              + 2 * ( qR_axi*wR_axi3 - qI_axi*wI_axi3 - qR_axi*wI_axi*wR_axi ) / q_axi2
              + qR_axi*chip * ( wI_axi2 - wR_axi2 ) / (BB*q_axi2) + 6*wI_axi*wR_axi * ( wR_axi*qI_axi - wI_axi*qR_axi ) / (BB*q_axi2)
              + 2 * ( qR_axi*wR_axi3 - qI_axi*wI_axi3 - qI_axi*chip*wI_axi*wR_axi ) / (BB*q_axi2);

	/*-----------------------------------------------------*/
	CCTK_REAL C4R_axi, C4I_axi;

	C4R_axi = - q_axi2 + 2 * ( qR_axi2 - qR_axi*wI_axi + wI_axi2 - wR_axi2 - qI_axi*wR_axi )
              + chip * ( qI_axi + 2*wR_axi ) / BB
              + 4 * ( wI_axi2 - wR_axi2 ) / BB - 2 * ( qI_axi*wR_axi + qR_axi*wI_axi )/ BB
              - ( wR_axi4 + wI_axi4 ) / q_axi2 - 2 * ( qR_axi*wI_axi3 + qI_axi*wR_axi3 ) / q_axi2
              + 6*wI_axi*wR_axi * ( wI_axi*qI_axi + wR_axi*qR_axi + wI_axi*wR_axi ) / q_axi2
              + qI_axi*chip * ( wR_axi2 - wI_axi2 ) / (BB*q_axi2)
              + 6*wI_axi*wR_axi * ( wI_axi*qI_axi + wR_axi*qR_axi ) / (BB*q_axi2)
              - 2 * ( qR_axi*wI_axi3 + qI_axi*wR_axi3 + qR_axi*chip*wI_axi*wR_axi ) / (BB*q_axi2)
              + 2*qR_axi2 * ( wI_axi4 + wR_axi4 - 6*wI_axi2*wR_axi2 ) / q_axi4
              + 8*qI_axi*qR_axi*wI_axi*wR_axi * ( wR_axi2 - wI_axi2 ) / q_axi4;

	C4I_axi =   2 * ( qI_axi*qR_axi + qR_axi*wR_axi - qI_axi*wI_axi - 2*wI_axi*wR_axi )
              + chip * ( 2*wI_axi - qR_axi ) / BB + 2 * ( qR_axi*wR_axi - qI_axi*wI_axi - 4*wI_axi*wR_axi ) / BB
              + 2*wI_axi3 * ( qI_axi + 2*wR_axi ) / q_axi2 - 2*wR_axi3 * ( qR_axi + 2*wI_axi ) / q_axi2
              + 6*wI_axi*wR_axi * ( wI_axi*qR_axi - wR_axi*qI_axi ) / q_axi2
              + qR_axi*chip * ( wR_axi2 - wI_axi2 ) / (BB*q_axi2)
              + 6*wI_axi*wR_axi * ( wI_axi*qR_axi - wR_axi*qI_axi ) / (BB*q_axi2)
              + 2 * ( qI_axi*wI_axi3 + qI_axi*chip*wI_axi*wR_axi - qR_axi*wR_axi3 ) / (BB*q_axi2)
              - 2*qI_axi*qR_axi * ( wI_axi4 + wR_axi4 ) / q_axi4 + 12*qI_axi*qR_axi*wI_axi2*wR_axi2 / q_axi4
              + 8*qR_axi2*wI_axi*wR_axi * ( wR_axi2 - wI_axi2 ) / q_axi4;

	/*-----------------------------------------------------*/

	/*--- alpha, beta, gamma, Eqs. 37 - 39 in Dolan '07 ---*/
	// alpha
	CCTK_REAL alphR_axi[n_sum], alphI_axi[n_sum], alph_axi2[n_sum];
	for( int m = 0; m < n_sum; ++m )
	{

	  alphR_axi[m] = ( C0R_axi + m ) * ( m + 1 );
          alphI_axi[m] = C0I_axi	 * ( m + 1 );
	  alph_axi2[m] = alphR_axi[m]*alphR_axi[m] + alphI_axi[m]*alphI_axi[m];

	}

	/*-----------------------------------------------------*/
	// beta
	CCTK_REAL betaR_axi[n_sum], betaI_axi[n_sum];
	for( int m = 0; m < n_sum; ++m )
	{

          betaR_axi[m] = C3R_axi + m * ( C1R_axi + 2*(1-m) );
          betaI_axi[m] = C3I_axi + m * C1I_axi;

	}

	/*-----------------------------------------------------*/
	// gamma
    	CCTK_REAL gamR_axi[n_sum], gamI_axi[n_sum];
	for( int m = 0; m < n_sum; ++m )
	{

          gamR_axi[m] = C4R_axi + m * ( C2R_axi + m - 3 );
          gamI_axi[m] = C4I_axi + m * C2I_axi;

	}

	/*-----------------------------------------------------*/

	/*--- AnR, AnI, Eqs. 35,36 in Dolan '07 ---------------*/
	CCTK_REAL AnR_axi[n_sum], AnI_axi[n_sum];

	AnR_axi[0] = A0_axi;
	AnI_axi[0] = 0.0;

	AnR_axi[1] = - AnR_axi[0] * ( alphR_axi[0] * betaR_axi[0] + alphI_axi[0] * betaI_axi[0] ) / alph_axi2[0];
	AnI_axi[1] = - AnR_axi[0] * ( alphR_axi[0] * betaI_axi[0] - alphI_axi[0] * betaR_axi[0] ) / alph_axi2[0];

	for( int m = 2; m < n_sum; ++m)
	{

	   AnR_axi[m] = ( - AnR_axi[m-1] * ( alphR_axi[m-1] * betaR_axi[m-1] + alphI_axi[m-1] * betaI_axi[m-1] )
		      + AnI_axi[m-1] * ( alphR_axi[m-1] * betaI_axi[m-1] - alphI_axi[m-1] * betaR_axi[m-1] )
		      - AnR_axi[m-2] * ( alphR_axi[m-1] * gamR_axi[m-1]  + alphI_axi[m-1] * gamI_axi[m-1]  )
		      + AnI_axi[m-2] * ( alphR_axi[m-1] * gamI_axi[m-1]  - alphI_axi[m-1] * gamR_axi[m-1]  )
		    ) / alph_axi2[m-1];

	   AnI_axi[m] = ( - AnR_axi[m-1] * ( alphR_axi[m-1] * betaI_axi[m-1] - alphI_axi[m-1] * betaR_axi[m-1] )
		      - AnI_axi[m-1] * ( alphR_axi[m-1] * betaR_axi[m-1] + alphI_axi[m-1] * betaI_axi[m-1] )
		      - AnR_axi[m-2] * ( alphR_axi[m-1] * gamI_axi[m-1]  - alphI_axi[m-1] * gamR_axi[m-1]  )
		      - AnI_axi[m-2] * ( alphR_axi[m-1] * gamR_axi[m-1]  + alphI_axi[m-1] * gamI_axi[m-1]  ) 
		    ) / alph_axi2[m-1];

	}

	/*-----------------------------------------------------*/
     // X4R, X4I 
     X4R_axi = 0.0;
     X4I_axi = 0.0;

     for( int m = 0; m < n_sum; ++m )
     {

       X4R_axi = X4R_axi + AnR_axi[m] * pow( Rp / Rm, m );
       X4I_axi = X4I_axi + AnI_axi[m] * pow( Rp / Rm, m );

     }
     /*========================================================*/

     /*=== initialize R_{11} spherical harmonics ( in cartesian coordinates ) ===*/

     RR11_axi =  X1I_axi * ( X2I_axi*X3I_axi*X4I_axi - X2R_axi*X3R_axi*X4I_axi - X2R_axi*X3I_axi*X4R_axi - X2I_axi*X3R_axi*X4R_axi )
           - X1R_axi * ( X2R_axi*X3I_axi*X4I_axi + X2I_axi*X3R_axi*X4I_axi + X2I_axi*X3I_axi*X4R_axi - X2R_axi*X3R_axi*X4R_axi );

     RI11_axi =  X1R_axi * (-X2I_axi*X3I_axi*X4I_axi + X2R_axi*X3R_axi*X4I_axi + X2R_axi*X3I_axi*X4R_axi + X2I_axi*X3R_axi*X4R_axi )
           - X1I_axi * ( X2R_axi*X3I_axi*X4I_axi + X2I_axi*X3R_axi*X4I_axi + X2I_axi*X3I_axi*X4R_axi - X2R_axi*X3R_axi*X4R_axi );

     /*========================================================*/

     /*=== s=0 spheroidal harmonics ===========================*/
     // Note: see Eqs. (2.2) - (2.9) in gr-qc/0511111
	/*-----------------------------------------------------*/
	CCTK_REAL cR_axi, cI_axi;
	if( wR_axi2 < axi_mu2 )
	{
	  cR_axi = chip * wI_axi * wR_axi / sqrt( axi_mu2 - wR_axi2 );
	  cI_axi = chip * ( axi_mu4 + wR_axi4 + 0.5 * axi_mu2 * ( wI_axi2 - 4.0*wR_axi2 ) ) / sqrt( pow( axi_mu2 - wR_axi2, 3 ) );
	}
	else if( axi_mu2 < wR_axi2 )
	{
	  cR_axi = chip * ( axi_mu4 + wR_axi4 + 0.5 * axi_mu2 * ( wI_axi2 - 4.0*wR_axi2 ) ) / sqrt( pow( wR_axi2 - axi_mu2, 3 ) );
	  cI_axi = chip * wI_axi * wR_axi / sqrt( wR_axi2 - axi_mu2 );
	}
     	else if( axi_mu2 == wR_axi2 && wI_axi > 0 )
	{
	  cR_axi =   chip * sqrt( wI_axi / axi_mu ) * ( axi_mu - 0.25*wI_axi );
	  cI_axi =   chip * sqrt( wI_axi / axi_mu ) * ( axi_mu + 0.25*wI_axi );
	}
     	else if( axi_mu2 == wR_axi2 && wI_axi < 0 )
	{
	  cR_axi =   chip * sqrt( abs(wI_axi) / axi_mu ) * ( axi_mu + 0.25*wI_axi );
	  cI_axi = - chip * sqrt( abs(wI_axi) / axi_mu ) * ( axi_mu - 0.25*wI_axi );
	}


	CCTK_REAL cR_axi2, cI_axi2;
	cR_axi2 = cR_axi * cR_axi;
	cI_axi2 = cI_axi * cI_axi;

	/*-----------------------------------------------------*/
	CCTK_REAL T1R_axi, T1I_axi;

        T1R_axi = exp( cR_axi * CT ) * cos( cI_axi * CT );
        T1I_axi = exp( cR_axi * CT ) * sin( cI_axi * CT );

	/*-----------------------------------------------------*/
	//Note,  we redefine alph, beta, gam here!!!
	CCTK_REAL aa_axi[n_sum];
	CCTK_REAL bbR_axi[n_sum], bbI_axi[n_sum];
	CCTK_REAL ggR_axi[n_sum], ggI_axi[n_sum];

	for( int p = 0; p < n_sum; ++p )
	{

          aa_axi[p]  = -2.0 * ( p+1 ) * ( p+2 );
          bbR_axi[p] = 2.0 + cI_axi2 - 4 * cR_axi - cR_axi2
                     + 3 * p - 4 * cR_axi * p + p*p - LamR11_axi;
          bbI_axi[p] = -4 * cI_axi - 2 * cI_axi * cR_axi
                     - 4 * cI_axi * p - LamI11_axi;
          ggR_axi[p] = 2.0 * (p+1) * cR_axi;
          ggI_axi[p] = 2.0 * (p+1) * cI_axi;

	}

	/*-----------------------------------------------------*/
	CCTK_REAL ApR_axi[n_sum], ApI_axi[n_sum];

	ApR_axi[0] = Ap0_axi;
	ApI_axi[0] = 0.0;

	ApR_axi[1] = - ApR_axi[0] * bbR_axi[0] / aa_axi[0];
	ApI_axi[1] = - ApR_axi[0] * bbI_axi[0] / aa_axi[0];

	for( int p = 2; p < n_sum; ++p )
	{

	   ApR_axi[p] = ( - ApR_axi[p-1] * bbR_axi[p-1] + ApI_axi[p-1] * bbI_axi[p-1]
		      - ApR_axi[p-2] * ggR_axi[p-1] + ApI_axi[p-2] * ggI_axi[p-1]
		    ) / aa_axi[p-1];

	   ApI_axi[p] = ( - ApR_axi[p-1] * bbI_axi[p-1] - ApI_axi[p-1] * bbR_axi[p-1]
		      - ApR_axi[p-2] * ggI_axi[p-1] - ApI_axi[p-2] * ggR_axi[p-1]
		    ) / aa_axi[p-1];

	}

	/*-----------------------------------------------------*/
	CCTK_REAL  T2R_axi, T2I_axi;
        T2R_axi = 0.0;
        T2I_axi = 0.0;

	for( int p = 0; p < n_sum; ++p )
	{

          T2R_axi = T2R_axi + ApR_axi[p] * pow( 1 + CT, p );
          T2I_axi = T2I_axi + ApI_axi[p] * pow( 1 + CT, p );

	}

	/*-----------------------------------------------------*/
        // HW: TODO: double-check.
        // In Alex' implementation this is (1-CT)
        // In original (reviewed) implementation this is (1-CT2)
     SR11_axi = sqrt( 1.0 - CT2 ) * ( T1R_axi * T2R_axi - T1I_axi * T2I_axi );
     SI11_axi = sqrt( 1.0 - CT2 ) * ( T1R_axi * T2I_axi + T1I_axi * T2R_axi );

     /*========================================================*/

     /*=== initialize Psi =====================================*/
     // Set SF to zero inside horizon to avoid divergences near origin
     CCTK_REAL rH = ( 1.0 + BB ) / 4.0;

     if( rr < rH )
     {
       xi_re = 0.0;
       xi_im = 0.0;
     } 
     else
     {
       xi_re =  ( RR11_axi * SR11_axi - RI11_axi * SI11_axi ) * CP
              - ( RR11_axi * SI11_axi + RI11_axi * SR11_axi ) * SP;

       xi_im =  ( RR11_axi * SI11_axi + RI11_axi * SR11_axi ) * CP
              + ( RR11_axi * SR11_axi - RI11_axi * SI11_axi ) * SP;
     }

     /*========================================================*/

     /*=== calc momentum Kphi =================================*/
     // implement first part of Kphi
     // Kphi = -1/(2\alpha) ( \p_{t} - \Lie_{\beta} ) \phi
     // ASSUME FOR SIMPLICITY: \alpha=1, \beta^i = 0

     xit_re = - 0.5 * ( wR_axi * xi_im + wI_axi * xi_re );
     xit_im =   0.5 * ( wR_axi * xi_re - wI_axi * xi_im );

     /*========================================================*/

     /*=== write to grid functions ============================*/

     Theta_gf[ind]  = xi_re;
     KTheta_gf[ind] = xit_re;

     /*========================================================*/

    } /* for i */
   }  /* for j */
  }   /* for k */
/*=== end of loops over grid =================================*/
/*============================================================*/


  CCTK_INFO("=== End ID_AD_Axion_SBH_supradQBS initial data ===");

}
/* -------------------------------------------------------------------*/
