Canuda Arrangement : Canuda_Axidilaton
Author(s)    : Alexandru Dima, Chloe Richards, Helvi Witek (hwitek@illinois.edu)
Maintainer(s): Alexandru Dima, Chloe Richards, Helvi Witek
Licence    : LGPLv2+
--------------------------------------------------------------------------

1. Purpose:
=============
Canuda arrangement for numerical relativity simulations of scalar fields at decoupling - i.e., backreaction on the spacetime metric is neglected.

The arrangement can evolve a scalar field with even parity ( Phi ), as well as a pseudo-scalar field with odd parity ( Theta ).

NOTE: We refer to Phi as the dilaton, and similarly, we refer to Theta as the axion throughout the code. This naming convention 
      applies for all coupling choices. 

The scalar field equations both include self-interaction potentials, couplings to curvature-squared invariants and a nonlinear coupling between Phi and Theta.

The code can handle and evolve several scalar field models, e.g.:

- minimally-coupled field
- massive (pseudo)scalar
- Einstein-scalar-Gauss-Bonnet gravity
- dynamical Chern-Simons
- Axidilaton gravity

Details on model selection, coupling and potential functions can be found in the individual thorns

2. Arrangement overview:
==========================

- AxiDil_Base: thorn where grid functions are defined for the scalar fields and their momentum
- AxiDil_Init: thorn where initial data are implemented
- AxiDil_Evol: thorn that solve the time evolution of the fields


