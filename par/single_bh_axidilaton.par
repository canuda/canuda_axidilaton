
#------------------------------------------------------------------------------
ActiveThorns = "
  ADMBase
  ADMCoupling
  ADMMacros
  AEILocalInterp
  AHFinderDirect
  AxiDil_Base
  AxiDil_Evol
  AxiDil_Init
  Boundary
  Carpet
  CarpetIOASCII
  CarpetIOBasic
  CarpetIOHDF5
  CarpetIOScalar
  CarpetInterp
  CarpetLib
  CarpetMask
  CarpetReduce
  CarpetRegrid2
  CarpetTracker
  CartGrid3D
  CoordBase
  CoordGauge
  Fortran
  GenericFD
  GSL
  HDF5
  InitBase
  IOUtil
  KerrQuasiIsotropic
  LocalInterp
  LoopControl
  MoL
  Multipole
  NaNChecker
  NewRad
  QuasiLocalMeasures
  ReflectionSymmetry
  Slab
  SpaceMask
  SphericalSurface
  StaticConformal
  SymBase
  SystemStatistics
  TerminationTrigger
  Time
  TimerReport
  TmunuBase
"
#------------------------------------------------------------------------------


# Grid setup
#------------------------------------------------------------------------------

CartGrid3D::type                     = "coordbase"
Carpet::domain_from_coordbase        = yes
CoordBase::domainsize                = "minmax"

# make sure all (xmax - xmin)/dx are integers!
CoordBase::xmin                      =-128.0
CoordBase::ymin                      =-128.0
CoordBase::zmin                      =-128.0
CoordBase::xmax                      =+128.0
CoordBase::ymax                      =+128.0
CoordBase::zmax                      =+128.0
CoordBase::dx                        =  1.0
CoordBase::dy                        =  1.0
CoordBase::dz                        =  1.0

driver::ghost_size                   = 3

CoordBase::boundary_size_x_lower     = 3
CoordBase::boundary_size_y_lower     = 3
CoordBase::boundary_size_z_lower     = 3
CoordBase::boundary_size_x_upper     = 3
CoordBase::boundary_size_y_upper     = 3
CoordBase::boundary_size_z_upper     = 3

CoordBase::boundary_shiftout_x_lower = 0
CoordBase::boundary_shiftout_y_lower = 0
CoordBase::boundary_shiftout_z_lower = 0

ReflectionSymmetry::reflection_x     = no
ReflectionSymmetry::reflection_y     = no
ReflectionSymmetry::reflection_z     = no
ReflectionSymmetry::avoid_origin_x   = yes
ReflectionSymmetry::avoid_origin_y   = yes
ReflectionSymmetry::avoid_origin_z   = yes

Time::dtfac                             = 0.25
Carpet::time_refinement_factors         = "[1, 1, 2, 4, 8, 16, 32, 64, 128, 256, 512]"


# Mesh refinement
#------------------------------------------------------------------------------

Carpet::max_refinement_levels           = 7

CarpetRegrid2::num_centres              = 1

CarpetRegrid2::num_levels_1             = 7
CarpetRegrid2::radius_1[1]              =   64.0
CarpetRegrid2::radius_1[2]              =   12.0
CarpetRegrid2::radius_1[3]              =   6.0
CarpetRegrid2::radius_1[4]              =   3.0
CarpetRegrid2::radius_1[5]              =   1.5
CarpetRegrid2::radius_1[6]              =   0.75

Carpet::use_buffer_zones                = yes
Carpet::prolongation_order_space        = 5
Carpet::prolongation_order_time         = 2

CarpetRegrid2::freeze_unaligned_levels  = yes
CarpetRegrid2::regrid_every             = -1

CarpetRegrid2::verbose                  = no

Carpet::grid_structure_filename         = "carpet-grid-structure"
Carpet::grid_coordinates_filename       = "carpet-grid-coordinates"


# Initial Data
#------------------------------------------------------------------------------

ADMBase::initial_data                 = "KQI_ana"
ADMBase::initial_lapse                = "KQI_ana"
ADMBase::initial_shift                = "KQI_ana"
ADMBase::initial_dtlapse              = "zero"
ADMBase::initial_dtshift              = "zero"

ADMBase::lapse_timelevels             = 3
ADMBase::shift_timelevels             = 3
ADMBase::metric_timelevels            = 3

KerrQuasiIsotropic::m_plus            = 1.0
KerrQuasiIsotropic::spin_plus         = 0.9      # a/M = {0.1, 0.3, 0.5, 0.7, 0.9}
KerrQuasiIsotropic::pos_plus[0]       = 1.0d-04
KerrQuasiIsotropic::eps_r             = 1.0d-05

AxiDil_Init::set_lapse_to_zero    = "yes"  # Sets lapse to zero inside when r<=r_+
AxiDil_Base::AxiDil_initdata      = "ID_AD_Axidilaton_SBH" # uses Cano et al solution
AxiDil_Init::m_plus               = 1.0
AxiDil_Init::chi_plus             = 0.9     # a/M = {0.1, 0.3, 0.5, 0.7, 0.9}
AxiDil_Init::pos_plus[0]          = 1.0d-04
AxiDil_Init::eps_r                = 1.0d-05

InitBase::initial_data_setup_method   = "init_some_levels"
Carpet::init_fill_timelevels          = yes
Carpet::init_3_timelevels             = no

# Evolution
#------------------------------------------------------------------------------

ADMBase::evolution_method             = "static"
ADMBase::lapse_evolution_method       = "static"
ADMBase::shift_evolution_method       = "static"
ADMBase::dtlapse_evolution_method     = "static"
ADMBase::dtshift_evolution_method     = "static"

ADMBase::lapse_prolongation_type      = "none"
ADMBase::shift_prolongation_type      = "none"
ADMBase::metric_prolongation_type     = "none"

AxiDil_Base::evolution_method         = "AxiDil_Evol"
AxiDil_Base::RL                       = 1

AxiDil_Base::dil_coupling             = "exponential"
AxiDil_Base::aGB                      = 0.01  # aGB=as={0.001, 0.01, 0.1}
AxiDil_Base::dil_lambda               = 1.0
AxiDil_Base::dil_potential            = "none"

AxiDil_Base::AD_coupling              = "exponential"
AxiDil_Base::AD_lambda                = 1.0

AxiDil_Base::axi_coupling             = "linear"
AxiDil_Base::aCS                      = 0.01  # aCS=as={0.001, 0.01, 0.1}
AxiDil_Base::axi_potential            = "none"

AxiDil_Evol::impose_WW_floor          = yes
AxiDil_Evol::WW_floor                 = 1.0d-05
AxiDil_Evol::derivs_order             = 4
AxiDil_Evol::use_advection_stencils   = "yes"

# Spatial finite differencing
#------------------------------------------------------------------------------

AxiDil_Evol::apply_KO_dissipation = "yes"

AxiDil_Evol::dissipation_order = 5 

AxiDil_Evol::eps_phi0 = 0.001
AxiDil_Evol::eps_kphi0 = 0.001
AxiDil_Evol::eps_theta0 = 0.001
AxiDil_Evol::eps_ktheta0 = 0.001

# Integration method
#------------------------------------------------------------------------------
MoL::ODE_Method                 = "RK4"
MoL::MoL_Intermediate_Steps     = 4
MoL::MoL_Num_Scratch_Levels     = 1

Carpet::num_integrator_substeps = 4


# Spherical surfaces
#------------------------------------------------------------------------------

SphericalSurface::nsurfaces = 1
SphericalSurface::maxntheta = 66
SphericalSurface::maxnphi   = 124
SphericalSurface::verbose   = no

# Horizon
SphericalSurface::ntheta            [0] = 41
SphericalSurface::nphi              [0] = 80
SphericalSurface::nghoststheta      [0] = 2
SphericalSurface::nghostsphi        [0] = 2
CarpetMask::excluded_surface        [0] = 0
CarpetMask::excluded_surface_factor [0] = 1.0

CarpetMask::verbose = no


# Wave extraction
#------------------------------------------------------------------------------

Multipole::nradii       = 3
Multipole::radius[0]    =  10
Multipole::radius[1]    =  20
Multipole::radius[2]    =  50
Multipole::nphi         = 120
Multipole::ntheta       = 60
Multipole::integration_method = Simpson
Multipole::variables    = "
  AxiDil_Base::Phi_gf{sw=0 name='Phi'}
  AxiDil_Base::Theta_gf{sw=0 name='Theta'}
"

Multipole::l_max        = 4
Multipole::out_every    = 128
Multipole::output_hdf5  = no
Multipole::output_ascii = yes


# Horizons
#------------------------------------------------------------------------------

AHFinderDirect::verbose_level                            = "physics details"
AHFinderDirect::output_BH_diagnostics                    = "true"
AHFinderDirect::run_at_CCTK_POST_RECOVER_VARIABLES       = no

AHFinderDirect::N_horizons                               = 1
AHFinderDirect::find_every                               = 100000

AHFinderDirect::output_h_every                           = 0
AHFinderDirect::max_Newton_iterations__initial           = 50
AHFinderDirect::max_Newton_iterations__subsequent        = 50
AHFinderDirect::max_allowable_Theta_growth_iterations    = 10
AHFinderDirect::max_allowable_Theta_nonshrink_iterations = 10
AHFinderDirect::geometry_interpolator_name               = "Lagrange polynomial interpolation"
AHFinderDirect::geometry_interpolator_pars               = "order=4"
AHFinderDirect::surface_interpolator_name                = "Lagrange polynomial interpolation"
AHFinderDirect::surface_interpolator_pars                = "order=4"

AHFinderDirect::move_origins                             = no

AHFinderDirect::origin_x                             [1] = 0
AHFinderDirect::initial_guess__coord_sphere__x_center[1] = 0
AHFinderDirect::initial_guess__coord_sphere__radius  [1] = 0.25
AHFinderDirect::which_surface_to_store_info          [1] = 0
AHFinderDirect::set_mask_for_individual_horizon      [1] = no
AHFinderDirect::reset_horizon_after_not_finding      [1] = no
AHFinderDirect::max_allowable_horizon_radius         [1] = 3


# Isolated Horizons
#-------------------------------------------------------------------------------

QuasiLocalMeasures::verbose                = yes
QuasiLocalMeasures::veryverbose            = no
QuasiLocalMeasures::interpolator           = "Lagrange polynomial interpolation"
QuasiLocalMeasures::interpolator_options   = "order=4"
QuasiLocalMeasures::spatial_order          = 4
QuasiLocalMeasures::num_surfaces           = 1
QuasiLocalMeasures::surface_index      [0] = 0


# Check for NaNs
#-------------------------------------------------------------------------------

Carpet::poison_new_timelevels = no
CarpetLib::poison_new_memory  = no
Carpet::check_for_poison      = no

NaNChecker::check_every     = 512
NanChecker::check_after     = 0
NaNChecker::report_max      = 10
NaNChecker::verbose         = "all"
NaNChecker::action_if_found = "terminate"
NaNChecker::out_NaNmask     = yes
NaNChecker::check_vars      = "
  AxiDil_Base::Phi_gf
  AxiDil_Base::Theta_gf
"


# Timers
#-------------------------------------------------------------------------------

Cactus::cctk_timer_output               = "full"
TimerReport::out_every                  = 5120
TimerReport::n_top_timers               = 40
TimerReport::output_all_timers_together = yes
TimerReport::output_all_timers_readable = yes
TimerReport::output_schedule_timers     = no


# I/O thorns
#-------------------------------------------------------------------------------

Cactus::cctk_run_title       = $parfile
IO::out_dir                  = $parfile

IOScalar::one_file_per_group = yes
IOASCII::one_file_per_group  = yes
IOHDF5::one_file_per_group   = no
IOHDF5::use_checksums        = no

IOBasic::outInfo_every       = 64
IOBasic::outInfo_reductions  = "minimum maximum"
IOBasic::outInfo_vars        = "
  Carpet::physical_time_per_hour
  AxiDil_Base::Phi_gf
  AxiDil_Base::Theta_gf
"

# IOASCII output
IOASCII::output_symmetry_points = no
IOASCII::out3D_ghosts           = no

# output just at one point (0D)
IOASCII::out0D_every = 128
IOASCII::out0D_vars  = "
  Carpet::timing
  QuasiLocalMeasures::qlm_scalars{out_every = 100000}
  AxiDil_Base::Phi_gf
  AxiDil_Base::Theta_gf
  AxiDil_Evol::GBinv_gf{out_every = 100000}
  AxiDil_Evol::CSinv_gf{out_every = 100000}
"

# 1D text output
IOASCII::out1D_every            = 128
IOASCII::out1D_d                = no
IOASCII::out1D_x                = yes
IOASCII::out1D_y                = no
IOASCII::out1D_z                = yes
IOASCII::out1D_vars             = "
  ADMBase::lapse{out_every = 100000}
  ADMBase::shift{out_every = 100000}
  ADMBase::metric{out_every = 100000}
  ADMBase::curv{out_every = 100000}
  AxiDil_Base::Phi_gf
  AxiDil_Evol::AxiDil_TmnDil_Sca_gfs
  AxiDil_Evol::GBinv_gf{out_every = 100000}
  AxiDil_Base::Theta_gf
  AxiDil_Evol::AxiDil_TmnAxi_Sca_gfs
  AxiDil_Evol::CSinv_gf{out_every = 100000}
"

# remove for test runs
# 2D HDF5 output
IOHDF5::out2D_every             = 256
IOHDF5::out2D_xy                = yes
IOHDF5::out2D_xz                = yes
IOHDF5::out2D_yz                = no
IOHDF5::out2D_vars              = "
  AxiDil_Base::Phi_gf
  AxiDil_Evol::AxiDil_TmnDil_Sca_gfs
  AxiDil_Evol::GBinv_gf{out_every = 100000}
  AxiDil_Base::Theta_gf
  AxiDil_Evol::AxiDil_TmnAxi_Sca_gfs
  AxiDil_Evol::CSinv_gf{out_every = 100000}
"

Carpet::verbose                    = no
Carpet::veryverbose                = no
Carpet::schedule_barriers          = no
Carpet::storage_verbose            = no
CarpetLib::output_bboxes           = no

Cactus::cctk_full_warnings         = yes
Cactus::highlight_warning_messages = no


# Checkpointing and recovery
#-------------------------------------------------------------------------------
CarpetIOHDF5::checkpoint             = yes
IO::checkpoint_every_walltime_hours  = 24
IO::checkpoint_dir                   = "checkpoints"
IO::checkpoint_keep                  = 1
IO::checkpoint_ID                    = no
IO::checkpoint_on_terminate          = yes

IO::recover                          = "autoprobe"
IO::recover_dir                      = "checkpoints"

IO::abort_on_io_errors                      = yes
CarpetIOHDF5::open_one_input_file_at_a_time = yes
CarpetIOHDF5::compression_level             = 9


# Run termination
#-------------------------------------------------------------------------------
TerminationTrigger::max_walltime = 24.0
TerminationTrigger::on_remaining_walltime        = 30 # minutes
TerminationTrigger::output_remtime_every_minutes = 30

Cactus::terminate       = "time"
Cactus::cctk_final_time = 300.0  # sufficient for relaxation to stationary profile
